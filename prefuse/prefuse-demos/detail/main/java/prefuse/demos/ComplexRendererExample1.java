package prefuse.demos;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.util.Iterator;
import java.util.Random;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

import prefuse.Display;
import prefuse.Visualization;
import prefuse.action.ActionList;
import prefuse.action.RepaintAction;
import prefuse.action.assignment.ColorAction;
import prefuse.action.layout.RandomLayout;
import prefuse.controls.ControlAdapter;
import prefuse.controls.DragControl;
import prefuse.controls.PanControl;
import prefuse.controls.ZoomControl;
import prefuse.data.Graph;
import prefuse.data.Schema;
import prefuse.data.Tuple;
import prefuse.render.AbstractShapeRenderer;
import prefuse.render.DefaultRendererFactory;
import prefuse.render.Renderer;
import prefuse.util.ColorLib;
import prefuse.visual.VisualItem;

public class ComplexRendererExample1 {
  public static final String $rendererObj = "_RendererObject";
  public static final Schema renderObjSchema = new Schema();
  static {
    renderObjSchema.addColumn($rendererObj, JComponent.class);
  }

  public static void main (String[] argv) {
    Graph graph = new Graph();
    graph.addColumns(renderObjSchema);

    int numNodes = 4;
    for (int i = 0; i < numNodes; i++) {
      graph.addNode();
    }

    Random rand = new Random();

    for (int i = 0; i < numNodes; i++) {
      int first = rand.nextInt(numNodes);
      int second = rand.nextInt(numNodes);
      graph.addEdge(first, second);
    }

    Visualization vis = new Visualization();
    vis.add("graph", graph);
    vis.setRendererFactory(new DefaultRendererFactory(new JComponentRenderer()));
    Display d = new Display(vis);

    @SuppressWarnings("unchecked")
    Iterator<Tuple> nodes = graph.getNodes().tuples();
    while (nodes.hasNext()) {
      Tuple tuple = nodes.next();
      JComponent exampleComponent = getExampleComponent();
      d.add(exampleComponent);
      tuple.set($rendererObj, exampleComponent);
    }


    ColorAction edges = new ColorAction("graph.edges", VisualItem.STROKECOLOR, ColorLib.gray(200));
    ActionList color = new ActionList();
    color.add(edges);

    ActionList layout = new ActionList();
    layout.add(new RandomLayout("graph"));
    layout.add(new RepaintAction());

    ActionList animate = new ActionList(ActionList.INFINITY);
//    animate.add(new RepaintAction());

    vis.putAction("color", color);
    vis.putAction("layout", layout);
    vis.putAction("animate", animate);

    d.setSize(1000, 1000);
    d.addControlListener(new DragControl());
    d.addControlListener(new PanControl());
    d.addControlListener(new ZoomControl());
    d.addControlListener(new JComponentControlDispatcher());

    JFrame frame = new JFrame("prefuse example");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.getContentPane().add(d);
    frame.pack();
    frame.setVisible(true);

    displayComponentTestFrame();

    // We have to start the ActionLists that we added to the visualization
    vis.run("color");
    vis.run("layout");
    vis.runAfter("layout", "animate");
  }

  private static void displayComponentTestFrame () {
    JFrame frame = new JFrame("component example");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    final JComponent component = getExampleComponent();
//    frame.setGlassPane(new JPanel());
    frame.getGlassPane().setVisible(true);
    frame.getGlassPane().addMouseListener(new MouseAdapter() {
      private void dispatchEvent(MouseEvent e) {
        component.dispatchEvent(new MouseEvent(component,
            e.getID(),
            e.getWhen(),
            e.getModifiers(),
            e.getX(),
            e.getY(),
            e.getClickCount(),
            e.isPopupTrigger()));
      }

//      @Override public void mouseClicked (MouseEvent e) {
//        dispatchEvent(e);
//      }

      @Override public void mouseReleased (MouseEvent e) {
        dispatchEvent(e);
      }

//      @Override public void mouseEntered (MouseEvent e) {
//        dispatchEvent(e);
//      }
//
//      @Override public void mouseExited (MouseEvent e) {
//        dispatchEvent(e);
//      }
//
//      @Override public void mouseDragged (MouseEvent e) {
//        dispatchEvent(e);
//      }
//
//      @Override public void mouseMoved (MouseEvent e) {
//        dispatchEvent(e);
//      }
//
      @Override public void mousePressed (MouseEvent e) {
        dispatchEvent(e);
      }
    });
    frame.getContentPane().add(component);
    frame.pack();
    frame.setVisible(true);
  }

  static class JComponentRenderer2 extends AbstractShapeRenderer {

    @Override protected Shape getRawShape (VisualItem item) {
      JComponent component = (JComponent) item.getSourceTuple().get($rendererObj);
      Rectangle bounds = component.getBounds();
      bounds.translate((int) item.getX(), (int) item.getY());
      return bounds;
    }

  }

  static class JComponentRenderer implements Renderer {
    @Override public void render (Graphics2D g, VisualItem item) {
      renderMethod2(g, item);
    }

    private void renderMethod1 (Graphics2D g, VisualItem item) {
      JComponent component = (JComponent) item.getSourceTuple().get($rendererObj);
      Rectangle b = component.getBounds();
      int x = b.x + (int) item.getX() - b.width / 2;
      int y = b.y + (int) item.getY() - b.height / 2;
      int w = b.width;
      int h = b.height;
      component.setBounds(x, y, w, h);
      component.paint(g);
//      Graphics cg = g.create(x, y, w, h);
//      try {
//        component.paint(cg);
//      }
//      finally {
//        cg.dispose();
//      }
    }

    private void renderMethod2 (Graphics2D g, VisualItem item) {
      JComponent component = (JComponent) item.getSourceTuple().get($rendererObj);
      g.translate(item.getX(), item.getY());
      component.paint(g);
      g.translate(-item.getX(), -item.getY());
      component.setBounds(item.getBounds().getBounds());
    }

    private void renderMethod3 (Graphics2D g, VisualItem item) {
      JComponent component = (JComponent) item.getSourceTuple().get($rendererObj);
      AffineTransform oldPosition = g.getTransform();
      g.translate(item.getX(), item.getY());
      component.paint(g);
      g.setTransform(oldPosition);
    }

    @Override public boolean locatePoint (Point2D p, VisualItem item) {
      return item.getBounds().contains(p);
    }

    @Override public void setBounds (VisualItem item) {
      JComponent component = (JComponent) item.getSourceTuple().get($rendererObj);
      Rectangle bounds = component.getBounds();
      // table.setBounds((int)item.getX(), (int)item.getY(), (int)bounds.getWidth(), (int)bounds.getHeight());
      item.setBounds(item.getX(), item.getY(), bounds.getWidth(), bounds.getHeight());
    }

  }

  static JComponent getExampleComponent () {
    Random rand = new Random();

    String[] columnNames = { "First Name",
      "Last Name",
      "Sport",
      "# of Years",
      "Vegetarian" };

    Object[][] data = {
      { "Kathy", "Smith",
        "Snowboarding", rand.nextInt(10), rand.nextBoolean() },
      { "John", "Doe",
        "Rowing", rand.nextInt(10), rand.nextBoolean() },
      { "Sue", "Black",
        "Knitting", rand.nextInt(10), rand.nextBoolean() },
      { "Jane", "White",
        "Speed reading", rand.nextInt(100), rand.nextBoolean() },
      { "Joe", "Brown",
        "Pool", rand.nextInt(1000), rand.nextBoolean() }
    };

    JTable table = new JTable(data, columnNames);
     JScrollPane scrollPane = new JScrollPane(table);
    table.setFillsViewportHeight(true);

    JButton button = new JButton(new AbstractAction(Boolean.toString(rand.nextBoolean())) {
      @Override public void actionPerformed (ActionEvent e) {
        System.out.println("bla");
      }
    });
    JLabel label = new JLabel(Boolean.toString(rand.nextBoolean()));

     JPanel panel = new JPanel();
     panel.setPreferredSize(new Dimension(200, 200));
     panel.setBackground(Color.gray);
     panel.add(scrollPane);
//     panel.add(table);
    // panel.add(button);
    // panel.add(label);

    JComponent comp = table;

    layoutComponent(comp);
    comp.setDoubleBuffered(false);
    return comp;
  }

  static void layoutComponent (Component component) {
    component.setSize(component.getPreferredSize());

    synchronized (component.getTreeLock()) {
      component.doLayout();

      if (component instanceof Container) {
        for (Component child : ((Container) component).getComponents()) {
          layoutComponent(child);
        }
      }
    }
  }

  private static class JComponentControlDispatcher extends ControlAdapter {
    private void dispatchEvent(MouseEvent e, Component component) {
      Display display = (Display)e.getComponent();
      Component frame = SwingUtilities.getRoot(display);
      // FIXME: does not work with dragging, needs getAbsoluteCoordinate
//      component.dispatchEvent(SwingUtilities.convertMouseEvent(frame, e, component));
      Point2D p2d = display.getAbsoluteCoordinate(e.getPoint(), null);
      Point p = SwingUtilities.convertPoint(frame, (int) p2d.getX(), (int) p2d.getY(), component);
      component.dispatchEvent(new MouseEvent(component,
          e.getID(),
          e.getWhen(),
          e.getModifiers(),
          p.x,
          p.y,
          e.getClickCount(),
          e.isPopupTrigger()));

//      display.repaint();
//      display.repaint(component.getBounds());
//      display.repaintImmediate();
    }

//    @Override public void itemClicked (VisualItem item, MouseEvent e) {
//      JComponent component = (JComponent) item.getSourceTuple().get($rendererObj);
//      dispatchEvent(e, component);
////      component.dispatchEvent(e);
//    }

    @Override public void itemPressed (VisualItem item, MouseEvent e) {
      JComponent component = (JComponent) item.getSourceTuple().get($rendererObj);
      dispatchEvent(e, component);
      item.getVisualization().repaint();
    }

    @Override public void itemReleased (VisualItem item, MouseEvent e) {
      JComponent component = (JComponent) item.getSourceTuple().get($rendererObj);
      dispatchEvent(e, component);
      item.getVisualization().repaint();
    }

//    @Override public void itemClicked (VisualItem item, MouseEvent e) {
//      JComponent component = (JComponent) item.getSourceTuple().get($rendererObj);
//      dispatchEvent(e, component);
//    }

//    @Override public void itemEntered (VisualItem item, MouseEvent e) {
//      JComponent component = (JComponent) item.getSourceTuple().get($rendererObj);
//      dispatchEvent(e, component);
//    }

//    @Override public void itemExited (VisualItem item, MouseEvent e) {
//      JComponent component = (JComponent) item.getSourceTuple().get($rendererObj);
//      dispatchEvent(e, component);
//    }


  }
}