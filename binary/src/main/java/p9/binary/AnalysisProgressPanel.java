package p9.binary;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 *
 * @author raphael
 */
public class AnalysisProgressPanel extends JPanel {

  private final JComponent progressBar;
  private final JLabel progressLabel;
  
  private final JComponent barChart;
  private final JLabel iterationsLabel;
  private final JLabel addressesLabel;

  public AnalysisProgressPanel(JComponent progressBar, JLabel progressLabel,
          JComponent barChart) {
    super();
    this.progressBar = progressBar;
    this.progressLabel = progressLabel;
    this.barChart = barChart;
    
    this.iterationsLabel = new JLabel("<html><b>i<br/>t<br/>e<br/>r<br/>a<br/>t<br/>i<br/>o<br/>n<br/>s</b></html>");
    this.addressesLabel = new JLabel("<html><b>addresses →</b></html>");
    this.iterationsLabel.setHorizontalAlignment(SwingConstants.CENTER);
    this.addressesLabel.setHorizontalAlignment(SwingConstants.CENTER);
    
    initLayout();
  }

  private void initLayout() {
    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    add(progressBar);
    add(progressLabel);
    
    barChart.setPreferredSize(new Dimension(500, 170));
    iterationsLabel.setPreferredSize(new Dimension(25, 170));
    
    JPanel graph = new JPanel();
    graph.setLayout(new BorderLayout());
    graph.add(barChart, BorderLayout.CENTER);
    graph.add(iterationsLabel, BorderLayout.LINE_START);
    graph.add(addressesLabel, BorderLayout.PAGE_END);
    
    add(graph);
  }

  public class JVerticalLabel extends JComponent {

    private final String text;

    public JVerticalLabel(String s) {
      text = s;
    }

    @Override
    public void paintComponent(Graphics g) {
      super.paintComponent(g);
      Graphics2D g2d = (Graphics2D) g;

      g2d.rotate(Math.toRadians(270.0));
      g2d.drawString(text, 0, 0);
    }
  }
}
