package p9.binary.cfg;

import java.io.StringWriter;
import java.io.Writer;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

import p9.binary.cfg.graph.BasicBlock;
import p9.binary.cfg.graph.Cfg;
import javalx.data.Option;
import javalx.numeric.Interval;
import javalx.numeric.Range;
import rreil.disassembler.Instruction;
import rreil.disassembler.OperandPostorderIterator;
import rreil.disassembler.OperandTree;
import rreil.disassembler.OperandTree.Node;
import rreil.lang.RReil;
import rreil.lang.RReilAddr;
import rreil.lang.Rhs;
import rreil.lang.Rhs.Rvar;
import rreil.lang.lowlevel.LowLevelRReil;
import rreil.lang.util.RvarExtractor;
import bindead.analyses.Analysis;
import bindead.analyses.algorithms.data.CallString;
import bindead.debug.DomainQueryHelpers;
import bindead.domainnetwork.channels.SetOfEquations;
import bindead.domainnetwork.interfaces.RootDomain;
import bindead.domains.pointsto.PointsToSet;
import bindead.environment.platform.Platform;

import com.googlecode.jatl.Html;

/**
 * Some commonly used methods that deal with instructions.
 *
 * @author Bogdan Mihaila
 */
public class InstructionsHelper {

  public static RReil toRReil (Instruction insn) {
    RReil instruction = ((LowLevelRReil) insn).toRReil();
    return instruction;
  }

  /**
   * Return all the unique variables that occur in the given RREIL instruction.
   */
  public static Set<Rvar> getOccurringVariables (RReil instruction) {
    return RvarExtractor.getUnique(instruction);
  }

  /**
   * Return all the unique variables that occur in the given native instruction.
   * Uses the platform object to resolve names to registers and their sizes.
   */
  public static Set<Rvar> getOccurringVariables (Instruction nativeInstruction, Platform platform) {
    Set<Rvar> variables = new HashSet<>();
    for (String varName : getVariableNamesInNativeInstruction(nativeInstruction)) {
      Rvar variable = platform.getRegisterAsVariable(varName);
      if (variable != null)
        variables.add(variable);
    }
    return variables;
  }

  /**
   * Return the RREIL variable for a native register name,
   * e.g. the x86-32 register "al" is part of "eax", thus
   * we would return the RREIL variable "eax:8" for "al".
   */
  public static Option<Rvar> getNativeRegisterRReilVariable (String registerName, Platform platform) {
    return Option.fromNullable(platform.getRegisterAsVariable(registerName));
  }

  /**
   * Return all the unique variables that occur in the given native instruction.
   * It tries to match the names of variables in the RREIL instructions with names in the native instruction.
   * This might not work if the names do not match, e.g. the native register {@code ax} is translated
   * as {@code eax:16} in 32 bits RREIL.<br>
   * Unless you know what you want is this method use {@link #getOccurringVariables(Instruction, Platform)} instead.
   */
  public static Set<Rvar> getOccurringVariables (Instruction nativeInstruction,
      Collection<RReil> correspondingRReilInstructions) {
    Set<Rvar> variables = new HashSet<>();
    for (RReil insn : correspondingRReilInstructions) {
      variables.addAll(RvarExtractor.getUnique(insn));
    }
    return getVariablesOccurringInNativeInstruction(nativeInstruction, variables);
  }

  private static Set<String> getVariableNamesInNativeInstruction (Instruction insn) {
    Set<String> varNames = new HashSet<>();
    for (OperandTree operand : insn.operands()) {
      OperandPostorderIterator it = new OperandPostorderIterator(operand.getRoot());
      while (it.next()) {
        Node current = it.current();
        if (current.getType().equals(OperandTree.Type.Sym)) {
          String name = current.getData().toString();
          varNames.add(name);
        }
      }
    }
    return varNames;
  }

  private static Set<Rvar> getVariablesOccurringInNativeInstruction (Instruction insn, Set<Rvar> allVars) {
    Set<String> usedVarsNames = getVariableNamesInNativeInstruction(insn);
    List<Rvar> usedVars = new LinkedList<>();
    for (Rvar variable : allVars) {
      String name = variable.getRegionId().toString();
      if (usedVarsNames.contains(name))
        usedVars.add(variable);
    }
    return RvarExtractor.unique(usedVars);
  }

  @SuppressWarnings("rawtypes")
  public static String inoutStatesTooltip(Instruction instruction, Cfg cfg, BasicBlock block) {
    // prepare states
    // state before instruction:
    Option<RootDomain> stateBefore = getStateBeforeInsn(instruction, cfg);
    // state after instruction:
    Option<RootDomain> stateAfter = getStateAfterInsn(instruction, cfg, block);

    Analysis<?> analysis = cfg.getAnalysis().getAnalysis();
    RReilAddr selectedAddress = instruction.address();
    Set<Rhs.Rvar> variables;
    if (instruction instanceof LowLevelRReil) {
      variables = InstructionsHelper.getOccurringVariables(((LowLevelRReil) instruction).toRReil());
    } else {
      variables = InstructionsHelper.getOccurringVariables(instruction, analysis.getPlatform());
    }

    if (!variables.isEmpty()) {
      return statesToHTML(selectedAddress, stateBefore, stateAfter, variables);
    } else {
      return "";
    }
  }

  // procedures for typesetting the tooltip
  @SuppressWarnings("rawtypes")
  private static String statesToHTML (
      final RReilAddr address,
      final Option<RootDomain> stateBefore,
      final Option<RootDomain> stateAfter,
      final Set<Rhs.Rvar> variables) {

    final StringWriter htmlStringWriter = new StringWriter();
    new Html(htmlStringWriter) {
      {
        indent(indentOff); // needed as Swing Tooltips HTML seem to choke on tab indented HTML strings
        html();
        body();
        // headline (showing the address)
        p().style("padding-bottom: 3px;");
        i().text(address.toStringWithHexPrefix() + ":").end();
        end(); // p
        // state before
        u().text("Before").end();
        variableSetToHTML(stateBefore, variables, htmlStringWriter);
        br();
        // state after
        u().text("After").end();
        variableSetToHTML(stateAfter, variables, htmlStringWriter);
        end(); // body
        end(); // html
        done();
      }
    };
    return htmlStringWriter.toString();
  }

  /**
   * Returns the incoming state for an instruction.
   */
  @SuppressWarnings("rawtypes")
  public static Option<RootDomain> getStateBeforeInsn (Instruction insn, Cfg cfg) {
    Analysis<?> analysis = cfg.getAnalysis().getAnalysis();
    CallString callstring = cfg.getCallString();
    RReilAddr address = insn.address();
    return Option.fromNullable((RootDomain) analysis.getState(callstring, address).getOrNull());
  }

  /**
   * Returns the outgoing state for an instruction.
   */
  @SuppressWarnings("rawtypes")
  public static Option<RootDomain> getStateAfterInsn (Instruction insn, Cfg cfg, BasicBlock block) {
    Analysis<?> analysis = cfg.getAnalysis().getAnalysis();
    CallString callstring = cfg.getCallString();
    Option<RootDomain> stateAfter = Option.none();

    Iterator<Instruction> iter = block.iterator();
    // try to find #instr in the #block and obtain the instruction directly after #instr:
    while (iter.hasNext()) {
      if (iter.next() == insn) {
        try {
          RReilAddr nextInstructionAddress = iter.next().address();
          stateAfter =
            Option.fromNullable((RootDomain) analysis.getState(callstring, nextInstructionAddress).getOrNull());
        } catch (NoSuchElementException exc) {
          // Never mind! It seems that instr was just the last instruction from the block!
          // TODO: one could look at all the successor blocks and retrieve the state from the first insn there
        }
        break;
      }
    }
    return stateAfter;
  }

  @SuppressWarnings({"rawtypes", "unchecked"})
  public static String variableToHTML (final Option<RootDomain> state, final Rhs.Rvar variable) {
    StringWriter htmlWriter = new StringWriter();

    new Html(htmlWriter) {
      {
        indent(indentOff);
        html();
        body();

        p();
        // the variable name
        try {
          b().text(variable.getRegionId().toString()).end();
        } catch(NullPointerException e) {
          b().text("<unknown name>");
        }
        // the variable size and offset
        String sizeAnnotation = ":" + variable.getSize();
        if (variable.getOffset() != 0) {
          sizeAnnotation += "/" + variable.getOffset();
        }
        span().style("font-size: 70%;").text(sizeAnnotation).end();
        text(" = ");

        if (state.isSome()) {
          // the variables value (-range)
          Range range = DomainQueryHelpers.queryRange(variable, state.get());
          if (range == null) {
            text("<unknown>");
          } else {
            Interval value = range.convexHull();
            text(value.toString());
          }
        } else {
          text("<unknown>");
        }
        end();

        if (state.isSome()) {
          PointsToSet pointsTo = DomainQueryHelpers.queryPointsToSet(variable, state.get());
          if (pointsTo != null && !pointsTo.isEmpty()) {
            p();
            b().text("  pts: ").end();
            text(pointsTo.toString());
            end();
          }

          SetOfEquations equalities = DomainQueryHelpers.queryEqualities(variable, state.get());
          if (equalities != null && !equalities.isEmpty()) {
            p();
            b().text("  eqs: ").end();
            text(DomainQueryHelpers.formatLinearEqualities(equalities));
            end();
          }
        }
        // end body
        end();
        // end html
        end();
        done();
      }
    };
    return htmlWriter.toString();
  }

  @SuppressWarnings({"rawtypes", "unchecked"})
  private static Html variableSetToHTML (final Option<RootDomain> state, final Set<Rhs.Rvar> variables, Writer writer) {
    final String spacer = "&nbsp;&nbsp;&nbsp;";
    return new Html(writer) {
      {
        if (state.isNone()) {
          br();
          text("--");
        } else {
          for (Rhs.Rvar variable : variables) {
            br();
            StringBuilder builder = new StringBuilder();
            b().text(variable.getRegionId().toString()).end();
            String sizeAnnotation = ":" + variable.getSize();
            if (variable.getOffset() != 0)
              sizeAnnotation = sizeAnnotation + "/" + variable.getOffset();
            span().style("font-size: 70%;").text(sizeAnnotation).end();
            text(" = ");
            Range range = DomainQueryHelpers.queryRange(variable, state.get());
            if (range == null) {
              text("<unknown>");
            } else {
              Interval value = range.convexHull();
              text(value.toString());
            }
            PointsToSet pointsTo = DomainQueryHelpers.queryPointsToSet(variable, state.get());
            if (pointsTo != null && !pointsTo.isEmpty()) {
              b().raw(spacer).text("  pts: ").end();
              text(pointsTo.toString());
            }
            SetOfEquations equalities = DomainQueryHelpers.queryEqualities(variable, state.get());
            if (equalities != null && !equalities.isEmpty()) {
              b().raw(spacer).text("  eqs: ").end();
              text(DomainQueryHelpers.formatLinearEqualities(equalities));
            }
            text(builder.toString());
          }
        }
        br();
        done();
      }
    };
  }
}
