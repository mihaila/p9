package p9.binary.cfg.graph;

import java.util.Collection;

import rreil.disassembler.Instruction;
import rreil.lang.RReil;
import rreil.lang.RReilAddr;
import rreil.lang.lowlevel.LowLevelRReil;
import bindead.analyses.algorithms.data.CallString;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

/**
 * @author Bogdan Mihaila
 */
public class NativeCfg extends Cfg {
  private final RReilCfg rreilCfg;
  private Multimap<RReilAddr, RReil> instructionsMapping;

  public NativeCfg (AnalysisResult analysis, CallString callString, RReilCfg rreilCfg) {
    super(analysis, callString);
    this.rreilCfg = rreilCfg;
    buildRReilInstructionsMapping();
  }

  @Override public boolean isRReilCfg () {
    return false;
  }

  public RReilCfg getRReilCfg () {
    return rreilCfg;
  }

  /**
   * Build a mapping between native addresses and a set of RREIL instructions
   * that translate the native instruction at that address.
   */
  private void buildRReilInstructionsMapping () {
    instructionsMapping = HashMultimap.create();
    for (BasicBlock block : rreilCfg.getBasicBlocks()) {
      for (Instruction insn : block) {
        RReil instruction = ((LowLevelRReil) insn).toRReil();
        RReilAddr nativeAddress = instruction.getRReilAddress().withOffset(0);
        instructionsMapping.put(nativeAddress, instruction);
      }
    }
  }

  /**
   * Get all RREIL instructions that translate the given native instruction.
   */
  public Collection<RReil> getRReilInsns (Instruction insn) {
    return instructionsMapping.get(insn.address());
  }

}
