package p9.binary.cfg.controller;

import java.awt.event.MouseEvent;

import prefuse.controls.DragControl;
import prefuse.visual.EdgeItem;
import prefuse.visual.VisualItem;

/**
 * A drag control that suppresses actions on edges.
 */
public class DraggingControl extends DragControl {

  public DraggingControl () {
    this(true);
  }

  public DraggingControl (boolean repaint) {
    super(repaint);
  }

  @Override public void itemEntered (VisualItem item, MouseEvent e) {
    if (item instanceof EdgeItem)
      return;
    else
      super.itemEntered(item, e);
  }

  @Override public void itemExited (VisualItem item, MouseEvent e) {
    if (item instanceof EdgeItem)
      return;
    else
      super.itemExited(item, e);
  }

  @Override public void itemPressed (VisualItem item, MouseEvent e) {
    if (item instanceof EdgeItem)
      return;
    else
      super.itemPressed(item, e);
  }

  @Override public void itemReleased (VisualItem item, MouseEvent e) {
    if (item instanceof EdgeItem)
      return;
    else
      super.itemReleased(item, e);
  }

  @Override public void itemDragged (VisualItem item, MouseEvent e) {
    if (item instanceof EdgeItem)
      return;
    else
      super.itemDragged(item, e);
  }
}
