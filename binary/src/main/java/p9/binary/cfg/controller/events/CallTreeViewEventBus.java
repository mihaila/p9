package p9.binary.cfg.controller.events;

import bindead.analyses.algorithms.data.CallString;

import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;

import p9.binary.cfg.graph.AnalysisResult;

public class CallTreeViewEventBus {
  private static final CallTreeViewEventBus instance = new CallTreeViewEventBus();
  private final List<CallTreeViewEventListener> listeners = new ArrayList<CallTreeViewEventListener>();

  public static CallTreeViewEventBus getInstance () {
    return instance;
  }

  private CallTreeViewEventBus () {
  }

  public void publish (CallTreeFocusContextEvent event) {
    for (CallTreeViewEventListener listener : listeners) {
      listener.notify(event);
    }
  }

  public void removeListener (CallTreeViewEventListener listener) {
    listeners.remove(listener);
  }

  public void addListener (CallTreeViewEventListener listener) {
    listeners.add(0, listener);
  }

  public static String getEventsID (AnalysisResult analysis) {
    return analysis.getAnalyzedFileName();
  }

  public static interface CallTreeViewEventListener extends EventListener {
    public void notify (CallTreeFocusContextEvent event);
  }

  public static class CallTreeFocusContextEvent extends ViewEvent {
    private final CallString callstring;

    public CallTreeFocusContextEvent (AnalysisResult analysis, CallString callstring) {
      super(getEventsID(analysis));
      this.callstring = callstring;
    }

    public CallString getCallstring () {
      return callstring;
    }

  }

}
