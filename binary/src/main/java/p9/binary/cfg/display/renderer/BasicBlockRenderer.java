package p9.binary.cfg.display.renderer;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import p9.binary.cfg.display.ColorConfig;
import p9.binary.cfg.display.Overview;
import p9.binary.cfg.display.InstructionsSearcher.CfgBlockSearchHits;
import p9.binary.cfg.display.items.BasicBlockItem;
import p9.binary.cfg.display.items.DrawableItem;
import p9.binary.cfg.display.items.ItemSize;
import p9.binary.cfg.display.items.ItemsViewer;
import p9.binary.cfg.display.items.highlight.InstructionHighlight;
import p9.binary.cfg.display.items.instructions.InstructionItem;
import p9.binary.cfg.graph.BasicBlock;
import prefuse.Display;
import prefuse.render.AbstractShapeRenderer;
import prefuse.util.ColorLib;
import prefuse.util.FontLib;
import prefuse.util.GraphicsLib;
import prefuse.visual.VisualItem;
import rreil.disassembler.Instruction;
import bindead.debug.StringHelpers;

/**
 * A renderer for instruction basic blocks.
 */
public class BasicBlockRenderer extends AbstractShapeRenderer {
  private final ItemsViewer itemsViewer;
  private final Map<VisualItem, BasicBlockItem> blockItems;
  private final String blockSchemaID;
  private final String mousePositionSchemaID;
  private final String searchHitsSchemaID;
  private float borderWidth = 3;
  private int horizontalInsets;
  private int verticalInsets;
  private double arcWidth;
  private double arcHeight;
  private final RoundRectangle2D node = new RoundRectangle2D.Double();
  private final RoundRectangle2D dropShadow = new RoundRectangle2D.Double();
  private final Rectangle2D shape = new Rectangle2D.Double();
  private final Dimension boundingBox = new Dimension();
  private final BasicStroke borderStroke = new BasicStroke(borderWidth);
  private boolean printNumbersInHex;
  private VisualItem currentItem;
  private BasicBlockItem blockItem;
  private final boolean nativeBinary;

  public BasicBlockRenderer (ItemsViewer iv,
          Map<VisualItem, BasicBlockItem> blockItems, boolean displayNative,
          String blockSchemaID, String mousePositionSchemaID, String searchHitsSchemaID) {
    this.itemsViewer = iv;
    this.blockItems = blockItems;
    this.blockSchemaID = blockSchemaID;
    this.mousePositionSchemaID = mousePositionSchemaID;
    this.searchHitsSchemaID = searchHitsSchemaID;
    this.nativeBinary = displayNative;
    setRoundedCorners(10);
    setInsets(5, 5);
    setPrintNumbersInHex(true);
  }

  public final void setRoundedCorners (int radius) {
    setRoundedCorners(radius, radius);
  }

  public final void setRoundedCorners (int arcWidth, int arcHeight) {
    this.arcWidth = arcWidth;
    this.arcHeight = arcHeight;
  }

  public final void setBorderWidth (float borderWidth) {
    this.borderWidth = borderWidth;
  }

  public final float getBorderWidth () {
    return borderWidth;
  }

  public final void setInsets (int horizontal, int vertical) {
    horizontalInsets = horizontal;
    verticalInsets = vertical;
  }

  public int getHorizontalInset () {
    return horizontalInsets;
  }

  public int getVerticalInset () {
    return verticalInsets;
  }

  public final boolean isPrintNumbersInHex () {
    return printNumbersInHex;
  }

  public final void setPrintNumbersInHex (boolean printNumbersInHex) {
    this.printNumbersInHex = printNumbersInHex;
  }

  @Override public void render (Graphics2D g, VisualItem item) {
    double zoomFactor = item.getVisualization().getDisplay(0).getScale();
    if (zoomFactor < 0.20 || item.isInGroup(Overview.overviewGroup))
      renderOverviewQuality(g, item);
    else
      renderNormalQuality(g, item);
  }

  private void renderOverviewQuality (Graphics2D g, VisualItem item) {
    Shape iShape = getShape(item);
    if (iShape == null)
      return;
    drawBackground(g);
    drawBorder(g);
  }

  private void renderNormalQuality (Graphics2D g, VisualItem item) {
    Shape iShape = getShape(item);
    if (iShape == null)
      return;
    updateBlockDimensions();
    drawShadow(g);
    drawBackground(g);

    int textColor = currentItem.getTextColor();
    if (ColorLib.alpha(textColor) > 0) {
      // TODO: implement highlighting of the selected line
      CfgBlockSearchHits searchHits = getSearchHits();
      if (searchHits != null) {
        blockItem.setSearchQuery(searchHits.searchQuery, false);
        // TODO: there is currently no way to disable the highlight after this
        if (searchHits.selectedInstruction != null)
          blockItem.getViewer().highlight(new InstructionHighlight(searchHits.selectedInstruction));
      }
      else {
        blockItem.setSearchQuery(null, false); // delete any search result
      }
      blockItem.drawItem(g, new Point2D.Double(node.getMinX(), node.getMinY()));
    }
    drawBorder(g);
  }

  private void drawShadow (Graphics2D g) {
    int type = getRenderType(currentItem);
    if (type == RENDER_TYPE_FILL || type == RENDER_TYPE_DRAW_AND_FILL) {
      g.setPaint(ColorConfig.$NodeShadow);
      g.fill(dropShadow);
    }
  }

  /**
   * Uses {@code item.getFillColor()} to retrieve the color for the background.
   */
  private void drawBackground (Graphics2D g) {
    int type = getRenderType(currentItem);
    if (type == RENDER_TYPE_FILL || type == RENDER_TYPE_DRAW_AND_FILL) {
      GraphicsLib.paint(g, currentItem, node, getStroke(currentItem), RENDER_TYPE_FILL);
    }
  }

  /**
   * Uses {@code item.getStrokeColor()} to retrieve the color for the border.
   */
  private void drawBorder (Graphics2D g) {
    int type = getRenderType(currentItem);
    if (type == RENDER_TYPE_DRAW || type == RENDER_TYPE_DRAW_AND_FILL)
      GraphicsLib.paint(g, currentItem, node, borderStroke, RENDER_TYPE_DRAW);
  }

  @Override protected synchronized Shape getRawShape (VisualItem item) {
    installItemInRenderer(item);
    return shape;
  }

  private void installItemInRenderer (VisualItem item) {
    if (currentItem == item)
      return;
    currentItem = item;
    setBlock();
    setFont();
    updateBlockDimensions();
  }

  private int getNumberOfLines () {
    return blockItem.numberOfChildren();
  }

  /**
   * Recalculate the geometry of the block, especially height and width.
   *
   * BaseInstructionRenderer.calibrateWidths() has to be called before
   * this method in order to get correct results.
   */
  private void updateBlockDimensions () {
    double size = currentItem.getSize();

    double x = currentItem.getX();
    double y = currentItem.getY();
    // safety checks
    if (Double.isNaN(x) || Double.isInfinite(x))
      x = 0;
    if (Double.isNaN(y) || Double.isInfinite(y))
      y = 0;

    ItemSize itemsSize = blockItem.getItemSize();

    // shadow offset
    int dx = 4;
    int dy = 4;

    node.setRoundRect(x, y, itemsSize.width, itemsSize.height,
            size * arcWidth, size * arcHeight);
    dropShadow.setRoundRect(x + dx, y + dy, itemsSize.width + dx,
            itemsSize.height + dy, size * arcWidth, size * arcHeight);

    // NOTE: this is a hack to extend the size of the node to also contain the
    // drop shadow. needed for the repaints
    double diffBoundsToContent = 8;
    shape.setRect(x, y, itemsSize.width + diffBoundsToContent,
            itemsSize.height + diffBoundsToContent);

    boundingBox.setSize(itemsSize);
  }

  private void setBlock () {
    BasicBlockItem storedItem = blockItems.get(currentItem);
    if (storedItem == null) { // lazy creation and caching
      BasicBlock block = (BasicBlock) currentItem.get(blockSchemaID);
      storedItem = new BasicBlockItem(block, nativeBinary, printNumbersInHex);
      storedItem.setPadding(DrawableItem.PADDING_HORIZONTAL, getHorizontalInset());
      storedItem.setPadding(DrawableItem.PADDING_VERTICAL, getVerticalInset());
      // set the viewer of the block
      storedItem.setViewer(itemsViewer);
      // register the newly created item so that we can reuse it
      blockItems.put(currentItem, storedItem);
    }
    blockItem = storedItem;
    updateBlockDimensions();
  }

  private void setFont () {
    double size = currentItem.getSize();
    Font font = currentItem.getFont();
    // scale the font as needed
    if (size != 1)
      font = FontLib.getFont(font.getName(), font.getStyle(), size * font.getSize());
    Color textColor = ColorLib.getColor(currentItem.getTextColor());

    blockItem.setFont(font);
    blockItem.setTextColor(textColor);
  }

  private CfgBlockSearchHits getSearchHits () {
    if (currentItem.canGet(searchHitsSchemaID, CfgBlockSearchHits.class))
      return (CfgBlockSearchHits) currentItem.get(searchHitsSchemaID);
    else
      return null;
  }

  /**
   * Returns the content of the item as a formatted string.
   */
  public String asString (VisualItem item) {
    installItemInRenderer(item); // initializes all the properties in this renderer
    List<InstructionItem> iitems = blockItem.getInstructionItems();
    return StringHelpers.joinWith("\n", InstructionItem.instructionItemsToStrings(iitems));
  }

  /**
   * Returns the content of the specified instruction in the item as a formatted string.
   */
  public String asString (VisualItem item, Instruction insn) {
    installItemInRenderer(item); // initializes all the properties in this renderer

    List<InstructionItem> iitems = blockItem.getInstructionItems();
    int indexCounter = 0;
    for (InstructionItem iitem : iitems) {
      if (iitem.getInstruction().equals(insn)) {
        break;
      }
      indexCounter++;
    }

    return InstructionItem.instructionItemsToStrings(iitems).get(indexCounter);
  }

  /**
   * Returns the content of the specified line in the item as a formatted string.
   */
  public String asString (VisualItem item, int line) {
    if (line > blockItem.numberOfChildren())
      return "";
    installItemInRenderer(item); // initializes all the properties in this renderer
    int lineNumber = 0;

    for(DrawableItem ditem: blockItem) {
      if(line == lineNumber) {
        return ditem.toString();
      }
    }

    return "";
  }

  /**
   * Pans the display animated to center on the line number in the item.
   */
  public void panToLine (int lineNumber, VisualItem item, Display display, int animationDuration) {
    installItemInRenderer(item);
    if (lineNumber < 0 || lineNumber > getNumberOfLines())
      return;

    DrawableItem selectedItem;
    Point2D itemPos;
    ItemSize itemSize;
    try {
      selectedItem = blockItem.getItemInLine(lineNumber-1);
      itemPos = blockItem.getPosition(selectedItem);
      itemSize = selectedItem.getItemSize();
    }
    catch(NullPointerException | NoSuchElementException e) {
      return;
    }

    double size = item.getSize();
    Rectangle2D bounds = item.getBounds();
    double x = bounds.getX() + itemPos.getX() + itemSize.getWidth() / 2;
    double y = bounds.getY() + size * verticalInsets + itemPos.getY() + itemSize.getHeight() / 2;
    display.animatePanToAbs(new Point2D.Double(x, y), animationDuration);
  }

  /**
   * Highlights a line in the item for a certain time and then removes the highlight.
   */
  public void highlightLine (int lineNumber, VisualItem item, int animationDuration) {
    installItemInRenderer(item);
    try {
      Instruction highlightedInstr = ((InstructionItem) blockItem.getItemInLine(lineNumber-1)).getInstruction();
      blockItem.getViewer().highlight(new InstructionHighlight(highlightedInstr));
    }
    catch(ClassCastException e) {}
    catch(NullPointerException e) {
      System.err.println("BasicBlockRenderer: line " + (lineNumber-1) +
              " cannot be highlighted: it does not exist!");
    }
  }

  public Point2D getMousePosition(VisualItem item) {
    installItemInRenderer(item);
    return getMousePosition();
  }

  private Point2D getMousePosition () {
    if (!currentItem.canGet(mousePositionSchemaID, Point2D.class))
      return null;
    Point2D mousePosition = (Point2D) currentItem.get(mousePositionSchemaID);
    if (mousePosition == null)
      return null;
    if (mousePosition.getX() < 0 && mousePosition.getY() < 0)
      return null;
    return mousePosition;
  }

  public BasicBlockItem getBlockItem () {
    return blockItem;
  }

}
