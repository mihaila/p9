package p9.binary.cfg.display.progressdialog;

import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Point2D;
import p9.binary.cfg.display.ColorConfig;
import p9.binary.cfg.display.StrokeConfig;
import rreil.lang.RReilAddr;

/**
 * Adds mouse interaction to an IterationsDiagram.
 * 
 * Decorates the IterationsDiagram with mouse interactivity.
 * 
 * @author Raphael Dümig
 */
public class InteractiveIterationsDiagram extends IterationsDiagram {
  
  private int hoveredIndex;
  private RReilAddr hoveredAddress;
  
  public InteractiveIterationsDiagram() {
    super();
    
    hoveredIndex = -1;
    hoveredAddress = null;
    
    IterationsDiagramMouseListener listener = new IterationsDiagramMouseListener();
    addMouseListener(listener);
    addMouseMotionListener(listener);
  }
  
  @Override
  public void paint(Graphics g) {
    super.paint(g);
    Graphics2D g2d = (Graphics2D) g;
    
    try {
      if (hoveredIndex != -1 && hoveredAddress != null) {
        Point2D addressPosition = getAddressPosition(hoveredIndex);
        
        // draw the vertical line for the address
        int addrLinePos = Math.round((float) addressPosition.getX());
        g2d.setStroke(StrokeConfig.$IterationsDiagramAddressMarker);
        g2d.setColor(ColorConfig.$IterationsDiagramAddressMarker);
        g2d.drawLine(addrLinePos, 0, addrLinePos, getHeight());
        
        String addrText = hoveredAddress.toShortStringWithHexPrefix();
        FontMetrics metrics = g.getFontMetrics(g.getFont());
        int addrTextWidth = metrics.stringWidth(addrText);
        
        if(addrLinePos + addrTextWidth < getWidth() - 5) {
          g2d.drawString(addrText, addrLinePos + 5, getHeight() - 5);
        }
        else {
          g2d.drawString(addrText, addrLinePos - 5 - addrTextWidth, getHeight() - 5);
        }
        
        // draw the horizontal line for the iterations
        int iterationsPos = Math.round((float) addressPosition.getY());
        g2d.setStroke(StrokeConfig.$IterationsDiagramAddressMarker);
        g2d.setColor(ColorConfig.$IterationsDiagramIterationsMarker);
        g2d.drawLine(0, iterationsPos, getWidth(), iterationsPos);
        String iterationsText = Integer.toString(getIterationCount(hoveredAddress));
        g2d.drawString(iterationsText, 5, iterationsPos - 5);
      }
    } catch (ArithmeticException e) {
      // probably division by zero, as there have not been added any addresses so far!
      return;
    }
  }
  
  
  class IterationsDiagramMouseListener implements MouseMotionListener, MouseListener {

    @Override
    public void mouseDragged(MouseEvent me) {
      return;
    }

    @Override
    public void mouseMoved(MouseEvent me) {
      hoveredIndex = Math.round(((float) me.getX()) / getWidth() * (getNumberOfAddresses() - 1));
      hoveredAddress = getAddressByIndex(hoveredIndex);
      repaint();
    }

    @Override
    public void mouseClicked(MouseEvent me) {
      return;
    }

    @Override
    public void mousePressed(MouseEvent me) {
      return;
    }

    @Override
    public void mouseReleased(MouseEvent me) {
      return;
    }

    @Override
    public void mouseEntered(MouseEvent me) {
      return;
    }

    @Override
    public void mouseExited(MouseEvent me) {
      hoveredIndex = -1;
    }
  }
  
}
