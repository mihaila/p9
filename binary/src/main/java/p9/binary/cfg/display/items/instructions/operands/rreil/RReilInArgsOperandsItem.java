package p9.binary.cfg.display.items.instructions.operands.rreil;

import java.util.List;

import p9.binary.cfg.display.items.ContainerItem;
import p9.binary.cfg.display.items.ListLayout;
import p9.binary.cfg.display.items.TextItem;
import rreil.lang.Rhs;
import static p9.binary.cfg.display.items.instructions.operands.rreil.RReilOperandItem.toItem;

/**
 *
 * @author Raphael Dümig <duemig@in.tum.de>
 */
public class RReilInArgsOperandsItem extends ContainerItem {
  
  public RReilInArgsOperandsItem(List<Rhs.Rval> inArgs, boolean numbersInHex) {
    super( new ListLayout(new TextItem(", "), new TextItem("("), new TextItem(")")) );
    
    for(Rhs arg: inArgs) {
      addItem(toItem(arg, numbersInHex));
    }
  }
}
