package p9.binary.cfg.display;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import prefuse.action.layout.Layout;
import prefuse.data.Graph;
import prefuse.render.RoutedEdgeRenderer;
import prefuse.visual.EdgeItem;
import prefuse.visual.NodeItem;

import com.sun.hotspot.igv.hierarchicallayout.HierarchicalLayoutManager;
import com.sun.hotspot.igv.layout.Cluster;
import com.sun.hotspot.igv.layout.LayoutGraph;
import com.sun.hotspot.igv.layout.Link;
import com.sun.hotspot.igv.layout.Port;
import com.sun.hotspot.igv.layout.Vertex;

/**
 * A hierarchical layout algorithm that is specialized to layout Control Flow Graphs.
 */
public class CfgLayout extends Layout {
  private static final String edgePathKey = RoutedEdgeRenderer.$EdgeRoutingKey;
  private static final int dropShadowLength = 8;

  public CfgLayout (String nodesID) {
    super(nodesID);
  }

  @Override public void run (double frac) {
    Graph g = (Graph) m_vis.getGroup(m_group);
    g.getEdges().addColumns(RoutedEdgeRenderer.$EdgeRoutingSchema);
    run(g);
  }

  private void run (Graph graph) {
    LayoutGraph layoutGraph = toLayoutGraph(graph);
    tweakLayoutParameters();
//    LinearLayoutManager layoutAlgorithm = new LinearLayoutManager(LinearLayoutManager.Combine.NONE);
    HierarchicalLayoutManager layoutAlgorithm = new HierarchicalLayoutManager(HierarchicalLayoutManager.Combine.NONE);
    layoutAlgorithm.doLayout(layoutGraph);
  }

//  private static void tweakLayoutParameters () {
//    LinearLayoutManager.SWEEP_ITERATIONS = 2;
//    LinearLayoutManager.CROSSING_ITERATIONS = 5;
//    LinearLayoutManager.DUMMY_HEIGHT = 1;
//    LinearLayoutManager.DUMMY_WIDTH = 1;
//    LinearLayoutManager.X_OFFSET = 20;
//    LinearLayoutManager.LAYER_OFFSET = 30;
//    LinearLayoutManager.MAX_LAYER_LENGTH = -1;
//    LinearLayoutManager.MIN_LAYER_DIFFERENCE = 1;
//  }

  private static void tweakLayoutParameters () {
    HierarchicalLayoutManager.SWEEP_ITERATIONS = 2;
    HierarchicalLayoutManager.CROSSING_ITERATIONS = 5;
    HierarchicalLayoutManager.DUMMY_HEIGHT = 1;
    HierarchicalLayoutManager.DUMMY_WIDTH = 1;
    HierarchicalLayoutManager.X_OFFSET = 20;
    HierarchicalLayoutManager.LAYER_OFFSET = 30;
    HierarchicalLayoutManager.MAX_LAYER_LENGTH = -1;
    HierarchicalLayoutManager.MIN_LAYER_DIFFERENCE = 1;
  }

  private LayoutGraph toLayoutGraph (Graph graph) {
    Set<Vertex> vertices = new HashSet<>();
    Set<Link> edges = new HashSet<>();
    Map<NodeItem, Vertex> nodesMapping = new HashMap<>();
    for (Iterator<?> nodes = graph.nodes(); nodes.hasNext();) {
      NodeItem nodeItem = (NodeItem) nodes.next();
      Vertex vertex = new VertexWrapper(nodeItem);
      vertices.add(vertex);
      nodesMapping.put(nodeItem, vertex);
    }
    for (Iterator<?> prefuseEdges = graph.edges(); prefuseEdges.hasNext();) {
      EdgeItem prefuseEdge = (EdgeItem) prefuseEdges.next();
      EdgeWrapper edge = new EdgeWrapper(prefuseEdge, nodesMapping);
      edges.add(edge);
    }
    return new LayoutGraph(edges, vertices);
  }

  private class VertexWrapper implements Vertex {
    private final NodeItem node;
    private Point position;
    private boolean isRoot;

    public VertexWrapper (NodeItem node) {
      this.node = node;
      this.position = new Point(0, 0);
      this.isRoot = false; // needs to be set later on when all edge connections are known
    }

    @Override public int compareTo (Vertex other) {
      // XXX bm: no clue what the semantics are.
      // This is taken from the CFG layout implementation of the hierarchical layout package
      return toString().compareTo(other.toString());
    }

    @Override public Cluster getCluster () {
      return null;
    }

    @Override public Dimension getSize () {
      Rectangle boundingBox = node.getBounds().getBounds();
      assert boundingBox != null : "null node-rectangle";
      return boundingBox.getSize();
    }

    @Override public Point getPosition () {
      return position;
    }

    @Override public void setPosition (Point p) {
      CfgLayout.this.setX(node, null, p.getX());
      CfgLayout.this.setY(node, null, p.getY());
      position = p;
    }

    @Override public boolean isRoot () {
      return isRoot;
    }

    protected void makeRoot () {
      isRoot = true;
    }

    @Override public String toString () {
      return node.toString();
    }
  }

  private class EdgeWrapper implements Link {
    private final Port source;
    private final Port target;
    private List<Point> points = new ArrayList<Point>();
    private final EdgeItem edge;

    public EdgeWrapper (EdgeItem edge, Map<NodeItem, Vertex> nodesMapping) {
      this.edge = edge;
      source = new PortWrapper(nodesMapping.get(edge.getSourceItem()), true);
      target = new PortWrapper(nodesMapping.get(edge.getTargetItem()), false);
    }

    @Override public Port getFrom () {
      return source;
    }

    @Override public Port getTo () {
      return target;
    }

    @Override public List<Point> getControlPoints () {
      return points;
    }

    @Override public void setControlPoints (List<Point> points) {
      edge.set(edgePathKey, points.toArray(new Point[] {}));
      this.points = points;
    }

  }

  private static class PortWrapper implements Port {
    final Vertex vertex;
    final Point relativePosition;

    public PortWrapper (Vertex vertex, boolean edgeSource) {
      this.vertex = vertex;
      // TODO: play around with these values and try to refine.
      int dy;
      if (edgeSource)
        dy = -dropShadowLength;
      else
        dy = 0;
      relativePosition = new Point(vertex.getSize().width / 2, dy);
    }

    @Override public Vertex getVertex () {
      return vertex;
    }

    @Override public Point getRelativePosition () {
      return relativePosition;
    }

  }
}
