
package p9.binary.cfg.display.items.instructions.operands.rreil;

import java.util.List;

import p9.binary.cfg.display.items.ContainerItem;
import p9.binary.cfg.display.items.ListLayout;
import p9.binary.cfg.display.items.TextItem;
import rreil.lang.Lhs;

/**
 *
 * @author Raphael Dümig <duemig@in.tum.de>
 */
public class RReilOutArgsOperandsItem extends ContainerItem {

  public RReilOutArgsOperandsItem(List<Lhs> outArgs, boolean numbersInHex) {
    super( new ListLayout(new TextItem(", "), new TextItem("<"), new TextItem(">")) );
    
    for(Lhs arg: outArgs)
      addItem(new RReilOperandItem(arg.asRvar(), numbersInHex));
  }
}
