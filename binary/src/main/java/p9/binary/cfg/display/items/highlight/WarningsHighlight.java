package p9.binary.cfg.display.items.highlight;

import java.util.Map.Entry;

import p9.binary.cfg.graph.Cfg;
import rreil.lang.RReilAddr;
import bindead.analyses.algorithms.data.CallString;
import bindead.analyses.algorithms.data.ProgramCtx;
import bindead.analyses.warnings.WarningsMap;
import bindead.domainnetwork.channels.WarningsContainer;
import bindead.domainnetwork.interfaces.ProgramPoint;

/**
 * @author Raphael Dümig
 */
public class WarningsHighlight implements DataHighlight<RReilAddr> {
  private final WarningsMap analyzerWarnings;
  private final CallString callstring;

  public WarningsHighlight (Cfg cfg) {
    if (cfg.isRReilCfg())
      this.analyzerWarnings = warningsOnlyForCfg(cfg.getAnalysis().getAnalysis().getWarnings(), cfg);
    else
      this.analyzerWarnings =
        warningsForNativeCfg(warningsOnlyForCfg(cfg.getAnalysis().getAnalysis().getWarnings(), cfg));
    this.callstring = cfg.getCallString();
  }

  /**
   * Determines the number of warnings at the address of the instruction.
   */
  public int numberOfWarnings (RReilAddr addr) {
    ProgramCtx location = new ProgramCtx(callstring, addr);
    return analyzerWarnings.get(location).size();
  }

  public WarningsContainer getWarningsCollection (RReilAddr addr) {
    ProgramCtx location = new ProgramCtx(callstring, addr);
    return analyzerWarnings.get(location);
  }

  public int getIterationCount (RReilAddr addr) {
    ProgramCtx location = new ProgramCtx(callstring, addr);
    return analyzerWarnings.getIterationOfWarnings(location);
  }

  @Override public boolean contains (RReilAddr addr) {
    ProgramCtx location = new ProgramCtx(callstring, addr);
    return !analyzerWarnings.get(location).isEmpty();
  }

  private static WarningsMap warningsOnlyForCfg (WarningsMap warningsMap, Cfg cfg) {
    CallString cfgCallstring = cfg.getCallString();
    WarningsMap result = new WarningsMap();
    for (Entry<ProgramPoint, WarningsContainer> entry : warningsMap.entrySet()) {
      ProgramPoint point = entry.getKey();
      if (point instanceof ProgramCtx && ((ProgramCtx) point).getCallString().equals(cfgCallstring))
        result.put(point, warningsMap.getIterationOfWarnings(point), entry.getValue());
    }
    return result;
  }

  private static WarningsMap warningsForNativeCfg (WarningsMap warningsMap) {
    WarningsMap result = new WarningsMap();
    for (Entry<ProgramPoint, WarningsContainer> entry : warningsMap.entrySet()) {
      ProgramPoint point = entry.getKey();
      ProgramPoint nativePoint = point.withAddress(point.getAddress().withOffset(0));
      WarningsContainer accumulatedWarnings = warningsMap.get(nativePoint).clone(); // original warnings at that point
      WarningsContainer existingCopiedWarnings = result.get(nativePoint); // some that might be already copied to that point
      accumulatedWarnings.addWarnings(existingCopiedWarnings);
      // if the current point is a native instruction that do not copy twice the warnings for this point
      if (!point.getAddress().isNative())
        accumulatedWarnings.addWarnings(entry.getValue());
      // NOTE: the iteration count is the one from the last copied warning.
      // As we accumulate warnings from different program points we need to pick some number
      result.put(nativePoint, warningsMap.getIterationOfWarnings(point), accumulatedWarnings);
    }
    return result;
  }

}
