package p9.binary.cfg.display;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javalx.data.products.P2;

import javax.swing.JToolBar;
import javax.swing.ToolTipManager;

import p9.binary.analysis.slicing.Slice;
import p9.binary.cfg.controller.BasicBlockHoverControl;
import p9.binary.cfg.controller.DraggingControl;
import p9.binary.cfg.controller.PanningControl;
import p9.binary.cfg.display.items.BasicBlockItem;
import p9.binary.cfg.display.items.ItemsViewer;
import p9.binary.cfg.display.items.highlight.DataHighlight;
import p9.binary.cfg.display.items.highlight.WarningsHighlight;
import p9.binary.cfg.display.renderer.BasicBlockRenderer;
import p9.binary.cfg.graph.BasicBlock;
import p9.binary.cfg.graph.Cfg;
import prefuse.Constants;
import prefuse.Display;
import prefuse.Visualization;
import prefuse.action.ActionList;
import prefuse.action.RepaintAction;
import prefuse.action.assignment.ColorAction;
import prefuse.action.assignment.FontAction;
import prefuse.action.layout.Layout;
import prefuse.controls.Control;
import prefuse.controls.MouseCoordinatesControl;
import prefuse.controls.NeighborHighlightControl;
import prefuse.controls.WheelZoomControl;
import prefuse.controls.ZoomToFitControl;
import prefuse.data.Node;
import prefuse.data.expression.parser.ExpressionParser;
import prefuse.render.DefaultRendererFactory;
import prefuse.render.RoutedEdgeRenderer;
import prefuse.visual.VisualItem;
import rreil.disassembler.Instruction;
import rreil.lang.RReilAddr;

public class CfgView extends Display implements ItemsViewer {
  public static final String $Font = "font";
  public static final String $Draw = "draw";
  public static final String $Layout = "layout";
  public static final String $Relayout = "relayout";
  public static final String $Repaint = "repaint";
  public static final String $Graph = "graph";
  public static final String $Nodes = $Graph + ".nodes";
  public static final String $Edges = $Graph + ".edges";
  private final boolean displayNative;
  private final Cfg cfg;
  private final Visualization visualization = m_vis;
  private Map<RReilAddr, P2<Node, Integer>> basicBlocks;
  private final Map<VisualItem, BasicBlockItem> blockItems;
  private WarningsHighlight warningsHighlight;
  private Slice slicingView;
  private JToolBar toolbar;
  private final Map<String, Component> toolbarItems;

  public CfgView(Cfg cfg) {
    this(cfg, false);
  }

  public CfgView(Cfg cfg, boolean displayNative) {
    super(new Visualization());
    this.cfg = cfg;
    this.displayNative = displayNative;
    this.blockItems = new WeakHashMap<>();
    this.warningsHighlight = null;
    this.toolbarItems = new HashMap<>();
    visualization.add($Graph, cfg);
    cfg.getNodeTable().addColumns(MouseCoordinatesControl.mousePositionSchema);
    cfg.getNodeTable().addColumns(InstructionsSearcher.searcherSchema);
    silenceParserMessages();
    initGraphDisplay();
    setBackground(ColorConfig.$Background);
    setForeground(ColorConfig.$Foreground);
  }

  @Override
  public Cfg getCfg() {
    return cfg;
  }

  void setToolbar (JToolBar toolbar) {
    this.toolbar = toolbar;
  }

  public void panToInstruction(RReilAddr address) {
    if (basicBlocks == null) {
      initBasicBlocksMap();
    }
    P2<Node, Integer> location = basicBlocks.get(address);
    if (location == null) {
      return;
    }
    VisualItem visualNode = visualization.getVisualItem($Nodes, location._1());
    BasicBlockRenderer renderer = (BasicBlockRenderer) visualNode.getRenderer();
    renderer.panToLine(location._2(), visualNode, this, 1000);
    renderer.highlightLine(location._2(), visualNode, 3000);
  }

  private void initBasicBlocksMap() {
    basicBlocks = new HashMap<>();
    for (Iterator<?> nodes = cfg.nodes(); nodes.hasNext();) {
      Node node = (Node) nodes.next();
      int index = 0;
      BasicBlock instructions = (BasicBlock) node.get(Cfg.$Block);
      for (Instruction insn : instructions) {
        RReilAddr address = insn.address();
        basicBlocks.put(address, new P2<>(node, index));
        index++;
      }
    }
  }

  private void initGraphDisplay() {
    initNodeAndEdgeRenderers();
    initDrawAction();
    initFontAction();
    initLayoutAction();
    initRelayoutAction();
    initDisplay();
    setLayout(new BorderLayout());
    runInitialActions();
  }

  private void initDisplay() {
    setHighQuality(true);
    addControlListener(new DraggingControl());
    addControlListener(new PanningControl());
    addControlListener(new WheelZoomControl());
    ZoomToFitControl zoomToFitControl = new ZoomToFitControl(Control.MIDDLE_MOUSE_BUTTON);
    zoomToFitControl.setMargin(10);
    addControlListener(zoomToFitControl);
    addControlListener(new NeighborHighlightControl()); // no need to repaint here, the control below will do the repaint
    addControlListener(new MouseCoordinatesControl($Repaint));
    addControlListener(new BasicBlockHoverControl(Cfg.$Block));
  }

  private void initNodeAndEdgeRenderers() {
    BasicBlockRenderer nodeRenderer
            = new BasicBlockRenderer(this, blockItems, displayNative, Cfg.$Block,
                    MouseCoordinatesControl.mousePositionKey, InstructionsSearcher.$SearchResult);
    RoutedEdgeRenderer edgeRenderer = new RoutedEdgeRenderer(Constants.EDGE_TYPE_POLYLINE_CURVE);
    edgeRenderer.setUpdateRoutePointsOnNodeMove(false);
    edgeRenderer.setDefaultLineWidth(1.5);
    edgeRenderer.setArrowHeadSize(10, 13);
    visualization.setRendererFactory(new DefaultRendererFactory(nodeRenderer, edgeRenderer));
  }

  private void initDrawAction() {
    ColorAction edgesDrawArrow = new ColorAction($Edges, VisualItem.FILLCOLOR, ColorConfig.$Edge);
    edgesDrawArrow.add(VisualItem.HIGHLIGHT, ColorConfig.$EdgeHighlight);
    ColorAction edgesDrawLine = new ColorAction($Edges, VisualItem.STROKECOLOR, ColorConfig.$Edge);
    edgesDrawLine.add(VisualItem.HIGHLIGHT, ColorConfig.$EdgeHighlight);
    ActionList draw = new ActionList();
    draw.add(new ColorAction($Nodes, VisualItem.STROKECOLOR, ColorConfig.$NodeBorder));
    draw.add(new ColorAction($Nodes, VisualItem.TEXTCOLOR, ColorConfig.$NodeFg));
    draw.add(new NodesFillColorAction());
    draw.add(edgesDrawArrow);
    draw.add(edgesDrawLine);
    visualization.putAction($Draw, draw);
    ActionList redraw = new ActionList();
    redraw.add(draw);
    redraw.add(new RepaintAction());
    visualization.putAction($Repaint, redraw);
  }

  private void initFontAction() {
    FontAction fontAction = new FontAction();
    fontAction.setDefaultFont(new Font(Font.MONOSPACED, Font.PLAIN, 11));
    visualization.putAction($Font, fontAction);
  }

  private void initLayoutAction() {
    ActionList layout = new ActionList();
    layout.add(getCurrentLayout());
    layout.add(new TranslateAction(40, 40, this));
    layout.add(new RepaintAction());
    visualization.putAction($Layout, layout);
  }

  private void initRelayoutAction() {
    ActionList layout = new ActionList();
    layout.add(getCurrentLayout());
    layout.add(new RepaintAction());
    visualization.putAction($Relayout, layout);
  }

  @Override
  public void relayout() {
    visualization.run($Relayout);
  }

  @Override
  public void redraw () {
    damageReport();
    visualization.run($Repaint);
  }

  @Override
  public boolean slicingEnabled() {
    return slicingView != null;
  }

  @Override public void applySlice (Slice slice) {
    // better use disableSlice for switching slicing off
    if (slice == null || slice.isEmpty()) {
      disableSlice();
    }
    slicingView = slice;
    for (BasicBlockItem blockItem : blockItems.values()) {
      blockItem.setSlice(slice);
    }
    redraw();
  }

  @Override
  public void disableSlice() {
    slicingView = null;
    for (BasicBlockItem blockItem : blockItems.values()) {
      blockItem.disableSlice();
    }
    redraw();
  }

  /**
   * isNativeView should be used to determine whether we are displaying a
   * binary in RReil or Native
   * 
   * @return true if this is a native view, false if rreil
   */
  public boolean isNativeView() {
    return displayNative;
  }
  
  private Layout getCurrentLayout () {
    // TODO: see how to get the currently selected layout and how to set it.
    return new CfgLayout($Graph);
  }

  private void runInitialActions() {
    visualization.run($Draw);
    visualization.run($Font);
    visualization.run($Layout);
  }

  @Override
  public void highlight(DataHighlight dataToHighlight) {
    for(BasicBlockItem bbi : blockItems.values()) {
      bbi.highlight(dataToHighlight);
    }
    redraw();
  }

  @Override
  public void disableHighlight(DataHighlight highlightedData) {
    for(BasicBlockItem bbi : blockItems.values()) {
      bbi.disableHighlight(highlightedData);
    }
    redraw();
  }

  @Override
  public void highlightWarnings() {
    warningsHighlight = new WarningsHighlight(cfg);
    highlight(warningsHighlight);
  }

  @Override
  public void disableWarningsHighlight() {
    try {
      disableHighlight(warningsHighlight);
    } catch(NullPointerException e) {}
    warningsHighlight = null;
  }

  @Override
  public boolean warningsHighlighted() {
    return warningsHighlight != null;
  }

  /**
   * Silence Prefuse parser info messages.
   */
  private static void silenceParserMessages() {
    Logger logger = Logger.getLogger(ExpressionParser.class.getName());
    logger.setLevel(Level.SEVERE);
  }

  @Override
  public void showTooltip(String tooltipText) {
    ToolTipManager.sharedInstance().setInitialDelay(500);
    ToolTipManager.sharedInstance().setDismissDelay(Integer.MAX_VALUE);
    setToolTipText(tooltipText);
  }

  @Override public void addToToolbar (String id, Component component) {
    toolbarItems.put(id, component);
    toolbar.add(component);
  }

  @Override public void removeFromToolbar (String id) {
    Component component = toolbarItems.get(id);
    if (component == null)
      return;
    toolbar.remove(component);
    toolbar.revalidate();
    toolbar.repaint();
  }

  public static class NodesFillColorAction extends ColorAction {

    public NodesFillColorAction() {
      super($Nodes, VisualItem.FILLCOLOR);
    }

    @Override
    public int getColor(VisualItem item) {
      if (item.isHover()) {
        return ColorConfig.$FocusBg;
      } else if (item.isHighlighted()) {
        return ColorConfig.$DOIBg;
      } else if (item.isFixed()) {
        return ColorConfig.$SearchBg;
      } else {
        return ColorConfig.$NodeBg;
      }
    }

  }

}
