package p9.binary.cfg.display.items;

import java.awt.geom.Point2D;
import java.util.NoSuchElementException;

/**
 *
 * @author Raphael Dümig <duemig@in.tum.de>
 */
public interface LayoutManager extends Iterable<DrawableItem> {
    /**
     * Append an item to the layout.
     * @param item the item to be appended
     */
    public void addItem(DrawableItem item);
    /**
     * Append a series of items to the layout.
     * @param items the series of items to be appended
     */
    public void addAllItems(Iterable<DrawableItem> items);
    /**
     * Insert an item between other elements in the layout.
     * @param item the item to be inserted
     * @param index the list index at which the element is inserted
     */
    public void insertItem(DrawableItem item, int index);
    /**
     * Remove the item at a given list index from the layout.
     * @param index the list index of the item to be removed
     */
    public void removeItem(int index);
    /**
     * Remove all items from the layout.
     * After this method call the layout will be empty.
     */
    public void removeAllItems();
    
    /**
     * Realign the items in the layout using their current size.
     */
    public void updateLayout();
    /**
     * Get the dimension of the bounding-box of the layout.
     * @return the dimension of the bounding-box of the layout
     */
    public ItemSize getLayoutSize();
    /**
     * Set the size of the layout to a dimension larger than the minimum
     * required size.
     * @param size the new size
     */
    public void setLayoutSize(ItemSize size);
    
    // get index returns the index of item in the layout
    // if item is not contained in the layout the result should be -1
    public int getIndex(DrawableItem item);
    
    public Point2D getPosition(DrawableItem item) throws NoSuchElementException;
    // get the position relative to the origin of the item
    public Point2D getRelativePos(Point2D position, DrawableItem item) throws NoSuchElementException;
    
    public DrawableItem getItemAt(Point2D position);
    
    // get the number of elements contained in this layout
    public int numberOfElements();
}
