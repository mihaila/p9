package p9.binary.cfg.display.items.highlight;

/**
 *
 * @author Raphael Dümig
 */
public interface DataHighlight<HighlightedType> {
  public boolean contains(HighlightedType itemData);
}
