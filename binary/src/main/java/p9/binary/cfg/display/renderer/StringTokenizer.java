package p9.binary.cfg.display.renderer;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A string tokenizer using regular expressions.
 *
 * @author Bogdan Mihaila
 */
public class StringTokenizer {

  /**
   * Split a string into tokens that match the given regular expression.
   */
  public static List<StringToken> split (String string, String splitterPattern) {
    Matcher matcher = Pattern.compile(splitterPattern).matcher(string);
    List<StringToken> tokens = new ArrayList<StringToken>();
    int currentPosition = 0;
    while (matcher.find()) {
      if (matcher.start() > currentPosition) {
        tokens.add(new StringToken(string.substring(currentPosition, matcher.start()), splitterPattern, false,
          TextRange.at(currentPosition, matcher.start())));
      }
      tokens.add(new StringToken(matcher.group(), splitterPattern, true, TextRange.at(matcher.start(), matcher.end())));
      currentPosition = matcher.end();
    }
    if (currentPosition < string.length())
      tokens.add(new StringToken(string.substring(currentPosition), splitterPattern, false, TextRange.at(
          currentPosition, string.length())));
    return tokens;
  }

  /**
   * A string token is a part of a string that matches or does not match a given regular expression.
   *
   * @author Bogdan Mihaila
   */
  public static class StringToken {
    public final String string;
    public final String regex;
    public final boolean isMatch;
    public final TextRange range;

    private StringToken (String string, String regex, boolean isMatch, TextRange range) {
      this.string = string;
      this.regex = regex;
      this.isMatch = isMatch;
      this.range = range;
    }


    @Override public String toString () {
      return string + " " + isMatch + " at " + range.toString() + " for " + regex;
    }
  }
}
