package p9.binary.cfg.display.items;

import java.awt.Component;

import p9.binary.analysis.slicing.Slice;
import p9.binary.cfg.display.items.highlight.DataHighlight;
import p9.binary.cfg.graph.Cfg;

/**
 * Interface to top level viewer.
 * Allows to modify or publish events to the viewer.
 *
 * @author Raphael Dümig
 */
public interface ItemsViewer {
  // GUI specific methods
  public void redraw();
  public void relayout();

  public void highlight(DataHighlight dataToHighlight);
  public void disableHighlight(DataHighlight highlightedData);
  public void showTooltip(String tooltipText);

  public void addToToolbar (String id, Component component);
  public void removeFromToolbar (String id);

  // Cfg specific methods
  public void applySlice(Slice slice);
  public boolean slicingEnabled();
  public void disableSlice();
  public Cfg getCfg();
  public void highlightWarnings();
  public void disableWarningsHighlight();
  public boolean warningsHighlighted();
}
