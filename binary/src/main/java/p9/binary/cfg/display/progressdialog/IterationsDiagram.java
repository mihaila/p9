package p9.binary.cfg.display.progressdialog;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;

import javax.swing.JPanel;

import rreil.lang.RReil;
import rreil.lang.RReilAddr;
import bindead.analyses.ProgressReporter;

import com.google.common.collect.SortedMultiset;
import com.google.common.collect.TreeMultiset;
import java.awt.geom.Point2D;
import java.util.NavigableSet;

/**
 * @author Raphael Dümig
 */
public class IterationsDiagram extends JPanel implements ProgressReporter.InstructionListener {

  private final SortedMultiset<RReilAddr> values;
  // the distinct addresses will be the values on the x-axis
  // values contains the same addresses multiple times (once for each iteration)
  private RReilAddr[] distinctValues;
  private boolean distinctUpdated;
  private ProgressReporter progressRepoter;
  private float stepWidth;
  private int maxValue;

  public IterationsDiagram () {
    values = TreeMultiset.create();
    
    // create an array containing the distinct addresses
    NavigableSet<RReilAddr> ns = values.elementSet();
    distinctValues = ns.toArray(new RReilAddr[ns.size()]);
    
    progressRepoter = null;
    distinctUpdated = false;
    setBackground(Color.white);
  }

  public void setProgressReporter(ProgressReporter progress) {
    progressRepoter = progress;
  }

  private void updateDistinctValues() {
    if (distinctUpdated) {
      // recreating the array each time is very inefficient, but currently its
      // size has to fit exactly as it is used for scaling the x-axis
      // TODO: improve this
      NavigableSet<RReilAddr> ns = values.elementSet();
      distinctValues = ns.toArray(new RReilAddr[ns.size()]);
      stepWidth = ((float) getWidth()) / (distinctValues.length-1);
      distinctUpdated = false;
    }
  }

  @Override public void paint (Graphics g) {
    super.paint(g);

    Graphics2D g2d = (Graphics2D) g;
    g2d.setColor(Color.WHITE);
    g2d.fillRect(0, 0, getWidth(), getHeight());
    g2d.setColor(Color.BLACK);

    int index = 0;
    int height = getHeight();
    synchronized (values) {
      updateDistinctValues();
      double lastX = 0;
      double lastY = height;
      double currentX;
      double currentY;
      for (RReilAddr addr : distinctValues) {
        int iterations = values.count(addr);
        currentX = stepWidth * index;
        currentY = height - iterations * getVerticalScale();
        g2d.draw(new Line2D.Double(lastX, lastY, currentX, currentY));
        lastX = currentX;
        lastY = currentY;
        index++;
      }
    }
  }

  @Override
  public void evaluatingInstruction(RReil insn) {
    if (progressRepoter == null)
      return;
    int iterations = progressRepoter.getIterationCount(insn);

    synchronized (values) {
      values.add(insn.getRReilAddress());
      if (iterations > maxValue) {
        maxValue = iterations;
      }
      if(iterations == 1) {
        distinctUpdated = true;
      }
    }

    repaint();
  }

  public int getNumberOfAddresses() {
    return distinctValues.length;
  }
  
  private float getVerticalScale() {
    return (0.9f * getHeight()) / maxValue;
  }
  
  /**
   * getAddressByIndex returns the i-th distinct address that appears in
   * the sorted set of addresses.
   * 
   * @param i the index of the address
   * @return the address object
   */
  public RReilAddr getAddressByIndex(int i) {
    return distinctValues[i];
  }
  
  public int getIterationCount(RReilAddr addr) {
    return values.count(addr);
  }
  
  public Point2D getAddressPosition(int addressIndex) {
    float addressPos = stepWidth * addressIndex;
    float iterationsPos = getHeight() - values.count(distinctValues[addressIndex]) * getVerticalScale();
    return new Point2D.Float(addressPos, iterationsPos);
  }
}
