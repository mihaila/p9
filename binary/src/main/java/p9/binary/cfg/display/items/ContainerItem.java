package p9.binary.cfg.display.items;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.util.Collection;
import java.util.Iterator;

import javax.swing.JPopupMenu;

import p9.binary.cfg.display.items.highlight.DataHighlight;

/**
 *
 * @author Raphael Dümig
 */
public abstract class ContainerItem extends DrawableItem {

  protected LayoutManager lm;
  private DrawableItem hoveredItem;
  
  private Font font;
  private Color tcolor;

  public ContainerItem(LayoutManager lm) {
    this.lm = lm;
    this.hoveredItem = null;
    this.font = null;
    this.tcolor = null;
  }

  public void addItem(DrawableItem di) {
    try {
      di.setViewer(root);
      di.setParent(this);
    } catch(NullPointerException e) {}
    lm.addItem(di);
  }

  public void addAllItems(Collection<DrawableItem> items) {
    for(DrawableItem di : items) {
      try {
        di.setViewer(root);
        di.setParent(this);
      } catch(NullPointerException e) {}
    }
    lm.addAllItems(items);
  }

  public void insertItem(DrawableItem di, int index) {
    try {
      di.setViewer(root);
      di.setParent(this);
    } catch(NullPointerException e) {}
    lm.insertItem(di, index);
  }

  public void removeItem(int index) {
    int n = 0;
    for(DrawableItem di : lm) {
      if(n >= index) {
        try {
          di.setViewer(null);
          di.setParent(null);
        } catch(NullPointerException e) {}
        break;
      }
    }

    lm.removeItem(index);
  }

  public void removeItem(DrawableItem item) {
    try {
      item.setViewer(null);
      item.setParent(null);
    } catch(NullPointerException e) {}
    lm.removeItem(lm.getIndex(item));
  }

  public void removeAllItems() {
    for(DrawableItem item : lm) {
      try {
        item.setViewer(null);
        item.setParent(null);
      } catch(NullPointerException e) {}
    }
    lm.removeAllItems();
  }

  @Override
  public Iterator<DrawableItem> iterator() {
    return lm.iterator();
  }

  public DrawableItem getItemAt(Point2D relPos) {
    return lm.getItemAt(getLayoutPosition(relPos));
  }

  public Point2D getLayoutPosition(DrawableItem item) {
    return lm.getPosition(item);
  }

  /**
   * Obtain the position of #item relative to the origin of this item
   * (including the parent item's padding).
   *
   * Item has to be a child of this item.
   *
   * @param item the child item whose position will be returned
   * @return the position of the child item relative to this item
   */
  public Point2D getPosition(DrawableItem item) {
    Point2D layoutPos = getLayoutPosition(item);
    return new Point2D.Double(layoutPos.getX() + getPadding(PADDING_LEFT),
                              layoutPos.getY() + getPadding(PADDING_TOP));
  }

  public int getLayoutIndex(DrawableItem item) {
    return lm.getIndex(item);
  }

  public int numberOfChildren() {
    return lm.numberOfElements();
  }

  @Override
  public ItemSize getCurrentSize() {
    return lm.getLayoutSize();
  }

  @Override
  public ItemSize getMinimumSize() {
    lm.updateLayout();
    return lm.getLayoutSize();
  }

  @Override
  public void setSize(ItemSize newSize) {
    super.setSize(newSize);
    lm.setLayoutSize(newSize);
  }

  @Override
  public void setFont(Font f) {
    font = f;
    
    for(DrawableItem d: lm) {
      try {
        d.setFont(f);
      } catch(NullPointerException e) {}
    }
  }

  @Override
  public Font getFont() {
    return this.font;
  }
  
  @Override
  public void setTextColor(Color col) {
    tcolor = col;
    
    for(DrawableItem d: lm) {
      try {
        d.setTextColor(col);
      } catch(NullPointerException e) {}
    }
  }

  @Override
  public Color getTextColor() {
    return this.tcolor;
  }
  
  @Override
  public void setSearchQuery(String query, boolean selected) {
    for(DrawableItem d: lm) {
      try {
        d.setSearchQuery(query, selected);
      } catch(NullPointerException e) {}
    }
  }

  @Override
  public void update() {
    lm.updateLayout();
    super.update();
  }

  @Override
  public void setViewer(ItemsViewer iv) {
    super.setViewer(iv);
    for(DrawableItem item : lm) {
      try {
        item.setViewer(iv);
      } catch(NullPointerException e) {}
    }
  }

  @Override public void draw (Graphics2D g, Point2D position) {
    for (DrawableItem item : lm) {
      if (item == null) {
        continue;
      }
      Point2D itemPos = lm.getPosition(item);
      if (itemPos == null) {
        continue;
      }
      item.drawItem(g, new Point2D.Double(position.getX() + itemPos.getX(), position.getY() + itemPos.getY()));
    }
  }

  @Override
  public void mouseMoved(MouseEvent me, Point2D relPos) {
    super.mouseMoved(me, relPos);
    
    // check if we have to pass the mouse event to any of the childs
    if(relPos == null) {
      return;
    }

    Point2D layoutPosition = getLayoutPosition(relPos);
    DrawableItem ditem = lm.getItemAt(layoutPosition);

    // call mouseExited and mouseEntered methods of the related children if we
    // are hovering a different child than the last time
    if(ditem != hoveredItem) {
      if(hoveredItem != null) {
        hoveredItem.mouseExited(me, lm.getRelativePos(layoutPosition, hoveredItem));
      }
      if(ditem != null) {
        ditem.mouseEntered(me, lm.getRelativePos(layoutPosition, ditem));
      }
      hoveredItem = ditem;
    }

    // pass event to the child that is currently hovered (if any)
    if(ditem != null)
      hoveredItem.mouseMoved(me, lm.getRelativePos(layoutPosition, hoveredItem));
  }

  @Override
  public void mouseExited(MouseEvent me, Point2D relPos) {
    super.mouseExited(me, relPos);
    
    if(hoveredItem != null) {

      if(relPos != null)
        hoveredItem.mouseExited(me, lm.getRelativePos(getLayoutPosition(relPos), hoveredItem));
      else
        hoveredItem.mouseExited(me, null);

      hoveredItem = null;
    }
  }
  
  @Override
  public void mouseClicked(MouseEvent me, Point2D relPos) {
    Point2D layoutPosition = getLayoutPosition(relPos);
    DrawableItem ditem = lm.getItemAt(layoutPosition);

    if (ditem != null) {
      ditem.mouseClicked(me, lm.getRelativePos(layoutPosition, ditem));
    }
  }

  @Override
  public JPopupMenu contextMenu(MouseEvent me, Point2D relPos) {
    if(relPos == null) return null;

    Point2D layoutPosition = getLayoutPosition(relPos);
    DrawableItem ditem = lm.getItemAt(layoutPosition);

    if(ditem != null)
      return ditem.contextMenu(me, lm.getRelativePos(layoutPosition, ditem));
    else
      return new JPopupMenu();
  }

  @Override public void highlight (DataHighlight dataToHighlight) {
    super.highlight(dataToHighlight);
    for (DrawableItem ditem : this) {
      if (ditem == null)
        continue;
      ditem.highlight(dataToHighlight);
    }
  }

  @Override public void disableHighlight (DataHighlight highlightedData) {
    super.disableHighlight(highlightedData);
    for (DrawableItem ditem : this) {
      if (ditem == null)
        continue;
      ditem.disableHighlight(highlightedData);
    }
  }
}
