package p9.binary.cfg.display.items.instructions.operands.x86;

import p9.binary.cfg.display.items.TextItem;
import rreil.disassembler.OperandTree;

/**
 *
 * @author Raphael Dümig
 */
public class PointerItem extends NativeOperandItem {
  private static final TextItem ptrstart = new TextItem("[");
  private static final TextItem ptrend = new TextItem("]");

  static {
    ptrstart.setPadding(PADDING_LEFT, 2);
    ptrstart.setPadding(PADDING_RIGHT, 3);
    ptrend.setPadding(PADDING_RIGHT, 2);
    ptrend.setPadding(PADDING_LEFT, 1);
  }

  private final int pointerSize;
  private final boolean isLea;

  public PointerItem(OperandTree pointerExpr, boolean hexadecimal, boolean lea) {
    super(pointerExpr, hexadecimal);

    this.pointerSize = ((Number) pointerExpr.getRoot().getData()).intValue();
    this.isLea = lea;
  }

  @Override
  public void updateOperands() {
    super.removeAllItems();

    if (!isLea) {
      super.addItem(new TextItem(sizeToString()));
    }
    super.addItem(ptrstart.clone());
    super.addAllItems(operandsToItems(operand, numbersInHex, true));
    super.addItem(ptrend.clone());
  }

  private String sizeToString() {
    switch (pointerSize) {
      case 8:
        return "BYTE PTR ";
      case 16:
        return "WORD PTR ";
      case 32:
        return "DWORD PTR ";
      case 64:
        return "QWORD PTR ";
      default:
        throw new IllegalArgumentException("The size of an x86 instruction cannot be " + pointerSize);
    }
  }
}
