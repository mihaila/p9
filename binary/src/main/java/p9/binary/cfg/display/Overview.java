package p9.binary.cfg.display;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.util.Iterator;

import javax.swing.BorderFactory;

import p9.binary.cfg.controller.FitOverviewListener;
import p9.binary.cfg.controller.OverviewControl;
import prefuse.Display;
import prefuse.Visualization;
import prefuse.data.Tuple;
import prefuse.util.display.DisplayLib;

/**
 * A birds eye overview display that highlights the current displayed part of the main display and allows for panning.
 * Code copied and improved from http://goosebumps4all.net/34all/bb/showthread.php?tid=221
 */
public class Overview extends Display {
  /**
   * The group of items that are shown in the overview renderer. This group is filled with all items before each repaint
   * and then again emptied after the paint. This way interested renderers can ask an item if it belongs to the overview
   * group and lower the render quality to achieve a better performance.
   */
  public static final String overviewGroup = "overview";
  private final Visualization visualization;

  public Overview (Display display) {
    super(display.getVisualization());
    visualization = getVisualization();
    setBackground(Color.white);
    setOpaque(false);
    setBorder(BorderFactory.createLineBorder(Color.black));
    DisplayLib.fitViewToBounds(this, visualization.getBounds(Visualization.ALL_ITEMS), 0);
    addItemBoundsListener(new FitOverviewListener());
    OverviewControl zoomToFitRectangleControl = new OverviewControl(display, this);
    addControlListener(zoomToFitRectangleControl);
    addPaintListener(zoomToFitRectangleControl);
    visualization.addFocusGroup(overviewGroup);
  }

  @Override public void paintDisplay (Graphics2D g2D, Dimension d) {
    addItemsToOverviewGroup();
    super.paintDisplay(g2D, d);
    removeItemsFromOverviewGroup();
  }

  @SuppressWarnings("unchecked") private void addItemsToOverviewGroup () {
    for (Iterator<Tuple> items = m_vis.items(); items.hasNext();) {
      Tuple item = items.next();
      visualization.getFocusGroup(overviewGroup).addTuple(item);
    }
  }

  @SuppressWarnings("unchecked") private void removeItemsFromOverviewGroup () {
    for (Iterator<Tuple> items = m_vis.items(); items.hasNext();) {
      Tuple item = items.next();
      visualization.getFocusGroup(overviewGroup).removeTuple(item);
    }
  }
}
