package p9.binary.cfg.display;

import java.awt.BasicStroke;

/**
 * define Stroke constants similar to ColorConfig defining Color constants
 *
 * @author Raphael Dümig
 */
public class StrokeConfig {

  public static final BasicStroke $ColumnSeparator = new BasicStroke(1);
  public static final BasicStroke $NativeInsnSeparator
          = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{5}, 0);
  public static final BasicStroke $IterationsDiagramAddressMarker
          = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{5}, 0);
}
