package p9.binary.cfg.display.items.highlight;

import rreil.disassembler.Instruction;

/**
 *
 * @author raphael
 */
public class InstructionHighlight implements DataHighlight<Instruction> {
  private final Instruction insn;

  public InstructionHighlight(Instruction insn) {
    this.insn = insn;
  }
  
  @Override
  public boolean contains(Instruction itemData) {
    return insn.equals(itemData);
  }
  
}
