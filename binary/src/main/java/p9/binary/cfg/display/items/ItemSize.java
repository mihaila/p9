package p9.binary.cfg.display.items;

import java.awt.geom.Dimension2D;

/**
 *
 * @author Raphael Dümig
 */
public class ItemSize extends Dimension2D {

  public double height;
  public double width;
  
  public ItemSize() {
    this(0.0, 0.0);
  }
  
  public ItemSize(double width, double height) {
    this.height = height;
    this.width = width;
  }
  
  @Override
  public double getWidth() {
    return width;
  }

  @Override
  public double getHeight() {
    return height;
  }

  @Override
  public void setSize(double width, double height) {
    this.height = height;
    this.width = width;
  }
  
  public boolean isLargerThan(ItemSize other) {
    return (width >= other.width) && (height >= other.height);
  }
}
