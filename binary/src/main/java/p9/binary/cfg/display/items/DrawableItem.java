package p9.binary.cfg.display.items;

import static prefuse.render.Renderer.DEFAULT_GRAPHICS;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Iterator;
import java.util.NoSuchElementException;

import javax.swing.JPopupMenu;

import p9.binary.cfg.display.items.highlight.DataHighlight;

/**
 *
 * @author Raphael Dümig <duemig@in.tum.de>
 */
public abstract class DrawableItem implements Cloneable, Iterable<DrawableItem> {

  public static final Graphics2D defaultContext = DEFAULT_GRAPHICS;

  public static final int PADDING_TOP = 0;
  public static final int PADDING_RIGHT = 1;
  public static final int PADDING_BOTTOM = 2;
  public static final int PADDING_LEFT = 3;
  public static final int PADDING_ALL = 4;
  public static final int PADDING_VERTICAL = 5;
  public static final int PADDING_HORIZONTAL = 6;

  protected ItemsViewer root = null;
  protected ContainerItem parent = null;
  protected ItemSize currentSize = null;
  protected Color background = null;
  protected int[] padding = {0, 0, 0, 0};
  protected boolean showBoarderRect = false;

  // These three methods are used to determine and set the size of the CONTENT
  // of the item.
  // Overwrite them if you want to build your own item and make them calculate
  // andd set the size of your content.
  // Do NOT use these methods to determine or set the size of the item itself.
  // Instead use getMinItemSize(), getItemSize() and setItemSize(ItemSize d).
  // The latter also take the border width into account and do some checks for
  // you.
  protected abstract ItemSize getMinimumSize();

  protected abstract void draw(Graphics2D g, Point2D position);

  public void drawItem(Graphics2D g, Point2D position) {
    Color oldColor = g.getColor();
    ItemSize size = getItemSize();

    // draw background
    if (background != null) {
      g.setColor(background);
      g.fill(new Rectangle2D.Double(position.getX(), position.getY(), size.width, size.height));
    }

    // draw boundary rectangle for debugging purposes
    if (showBoarderRect) {
      g.setColor(Color.BLACK);
      g.draw(new Rectangle2D.Double(position.getX(), position.getY(), size.width, size.height));
    }

    // reset the color
    g.setColor(oldColor);

    draw(g, new Point2D.Double( position.getX() + padding[PADDING_LEFT],
                                position.getY() + padding[PADDING_TOP] ));
  }

  public final void enableDebugging() {
    showBoarderRect = true;
  }

  public final void disableDebugging() {
    showBoarderRect = false;
  }

  public final ItemSize getMinItemSize() {
    ItemSize innerSize = getMinimumSize();
    return new ItemSize(innerSize.width + padding[PADDING_LEFT] + padding[PADDING_RIGHT],
            innerSize.height + padding[PADDING_TOP] + padding[PADDING_BOTTOM]);
  }

  public ItemSize getCurrentSize() {
    // size has not been set from outside? then use the smallest possible size
    if (currentSize == null) {
      currentSize = getMinimumSize();
    }

    return currentSize;
  }

  public final ItemSize getItemSize() {
    ItemSize itemSize = getCurrentSize();

    return new ItemSize(itemSize.width + padding[PADDING_LEFT] + padding[PADDING_RIGHT],
            itemSize.height + padding[PADDING_TOP] + padding[PADDING_BOTTOM]);
  }
  
  public double getItemHeight() {
    return getItemSize().getHeight();
  }
  
  public double getItemWidth() {
    return getItemSize().getWidth();
  }

  public Color getBackgroundColor() {
    return background;
  }

  public void setBackgroundColor(Color color) {
    background = color;
  }

  public void disableBackgroundColor() {
    background = null;
  }

  /**
   * setSize: Overwrite this method if you want to change the content of the
   * element when its size changes.
   * Make sure to call the super method when you overwrite this.
   *
   * @param size the new size of the element
   */
  protected void setSize(ItemSize size) {
    currentSize = (ItemSize) size.clone();
  }
  
  /**
   * Set the size to the minimum possible size of the item.
   */
  protected void updateSize () {
    currentSize = getMinimumSize();
  }

  public void update() {
    updateSize();
    if (parent != null) {
      parent.update();
    }
  }
  
  // only PADDING_TOP, PADDING_BOTTOM, PADDING_LEFT and PADDING_RIGHT implemented so far
  public final int getPadding(int position) {
    if (position < 4)
      return padding[position];
    else return -1;
  }

  public final void setPadding(int position, int value) {
    if (position < 4) {
      padding[position] = value;
    } else if (position == 4) {
      for (int p = 0; p < 4; p++) {
        padding[p] = value;
      }
    } else if (position == 5) {
      padding[0] = padding[2] = value;
    } else if (position == 6) {
      padding[1] = padding[3] = value;
    }
  }

  /** Subtract the padding of the item from the position.
   *
   * This can be helpful if you want to translate the coordinates to the coordinate
   * system of an embedded layout manager.
   *
   * @param position the position that has to be translated
   * @return the result of the translation
   */
  public final Point2D getLayoutPosition(Point2D position) {
    return new Point2D.Double( position.getX() - padding[PADDING_LEFT],
                               position.getY() - padding[PADDING_BOTTOM] );
  }

  public final void setItemSize(ItemSize proposedSize) {
    double newWidth = proposedSize.width - padding[PADDING_LEFT] - padding[PADDING_RIGHT];
    double newHeight = proposedSize.height - padding[PADDING_TOP] - padding[PADDING_BOTTOM];

    ItemSize minSize = getMinimumSize();
    if (newWidth < minSize.width) {
      newWidth = minSize.width;
    }
    if (newHeight < minSize.height) {
      newHeight = minSize.height;
    }

    setSize(new ItemSize(newWidth, newHeight));
  }
  
  public void setViewer(ItemsViewer iv) {
    root = iv;
  }
  
  public ItemsViewer getViewer() {
    return root;
  }
  
  public ContainerItem getParent() {
    return parent;
  }
  
  public void setParent(ContainerItem container) {
    parent = container;
  }

  @Override
  public DrawableItem clone() {
    try {
      return (DrawableItem) super.clone();
    } catch(CloneNotSupportedException e) {
      return null;
    }
  }

  @Override
  public Iterator<DrawableItem> iterator() {
    return new Iterator<DrawableItem>() {
      @Override public boolean hasNext() {
        return false;
      }

      @Override public DrawableItem next() {
        throw new NoSuchElementException();
      }

      @Override
      public void remove() {
        throw new IllegalStateException();
      }
    };
  }
  
  /**
   * Overwrite this to return a raw String representation of the items content.
   */
  @Override
  public String toString() {
    return "";
  }

  public void mouseEntered(MouseEvent me, Point2D relPos) {
    String tttext = tooltipText();
    if(!tttext.isEmpty() && root != null) {
      root.showTooltip(tttext);
    }
  }

  public void mouseExited(MouseEvent me, Point2D relPos) {
    // destroy the tooltip
    if(root != null) {
      root.showTooltip("");
    }
  }

  public void mouseMoved(MouseEvent me, Point2D relPos) {

  }

  public void mouseClicked(MouseEvent me, Point2D relPos) {

  }

  public JPopupMenu contextMenu(MouseEvent me, Point2D relPos) {
    return new JPopupMenu();
  }

  public String tooltipText() {
    return "";
  }
  
  /**
   * In case any text is displayed you can use this function to change its font.
   * This function call should be recursively passed to child elements.
   *
   * @param f the font to be applied
   */
  public void setFont(Font f) {

  }

  public Font getFont() {
    return null;
  }
  
  public void setTextColor(Color col) {

  }

  public Color getTextColor() {
    return null;
  }
  
  // override to allow highlighting of search queries
  public void setSearchQuery(String searchQuery, boolean selected) {

  }
  
  // Highlight all items that correspond to the data object #dataToHighlight.
  // All items may decide by themselves if they belong to the data.
  public void highlight(DataHighlight dataToHighlight) {
    
  }
  
  // disable the highlighting triggered by enableHighlight(highlightedData)
  public void disableHighlight(DataHighlight highlightedData) {
    
  }
  
}
