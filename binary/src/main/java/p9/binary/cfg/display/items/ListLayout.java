package p9.binary.cfg.display.items;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Raphael Dümig <duemig@in.tum.de>
 */
public class ListLayout extends HorizontalLayout {
  
  private final DrawableItem separator;
  private final DrawableItem startItem;
  private final DrawableItem endItem;
  
  private List<DrawableItem> items;
  
  public ListLayout(DrawableItem separator) {
    this(separator,null,null);
  }
  
  public ListLayout(DrawableItem start, DrawableItem end) {
    this(null,start,end);
  }
  
  public ListLayout(DrawableItem separator, DrawableItem start, DrawableItem end) {
    this.separator = separator;
    this.startItem = start;
    this.endItem = end;
    this.items = new LinkedList<>();
    
    updateListLayout();
  }
  
  @Override
  public void addItem(DrawableItem element) {
    items.add(element);
    updateListLayout();
  }
  
  @Override
  public void removeItem(int index) {
    items.remove(index);
    updateListLayout();
  }
  
  @Override
  public void addAllItems(Iterable<DrawableItem> allItems) {
    for(DrawableItem di : allItems) {
      items.add(di);
    }
    updateListLayout();
  }
  
  @Override
  public void removeAllItems() {
    items = new LinkedList<>();
    updateListLayout();
  }
  
  @Override
  public void insertItem(DrawableItem item, int index) {
    items.add(index, item);
    updateListLayout();
  }
  
  @Override
  public void replaceItem(DrawableItem item, int index) {
    items.set(index, item);
    updateListLayout();
  }
  
  @Override
  public boolean isEmpty() {
    return items.isEmpty();
  }
  
  @Override
  public int getIndex(DrawableItem item) {
    return items.indexOf(item);
  }
  
  @Override
  public int numberOfElements() {
    return items.size();
  }
  
  // rebuild the internal list that also contains the
  // start/end elements and the separators
  private void updateListLayout() {
    super.removeAllItems();
    
    super.addItem(startItem);
    boolean first = true;
    for(DrawableItem item: items) {
      if(first)
        first = false;
      else
        super.addItem(separator.clone());
      
      super.addItem(item);
    }
    super.addItem(endItem);
    
    updateLayout();
  }
}
