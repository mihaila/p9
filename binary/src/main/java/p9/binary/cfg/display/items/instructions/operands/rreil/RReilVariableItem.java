package p9.binary.cfg.display.items.instructions.operands.rreil;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

import javalx.data.Option;

import javax.swing.JCheckBox;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import p9.binary.analysis.slicing.Slice;
import p9.binary.analysis.slicing.Slicer;
import p9.binary.cfg.InstructionsHelper;
import p9.binary.cfg.display.ColorConfig;
import p9.binary.cfg.display.items.BasicBlockItem;
import p9.binary.cfg.display.items.DrawableItem;
import p9.binary.cfg.display.items.instructions.InstructionItem;
import p9.binary.cfg.graph.AnalysisResult;
import p9.binary.cfg.graph.BasicBlock;
import p9.binary.cfg.graph.Cfg;
import p9.binary.cfg.display.items.DrawableItem;
import p9.binary.cfg.graph.RReilCfg;
import rreil.disassembler.Instruction;
import rreil.lang.RReil;
import rreil.lang.Rhs.Rvar;
import rreil.lang.lowlevel.LowLevelRReil;
import bindead.domainnetwork.interfaces.RootDomain;

/**
 * @author Raphael Dümig
 */
public class RReilVariableItem extends RReilOperandItem {
  private final Rvar variable;

  public RReilVariableItem (Rvar operand, boolean numbersInHex) {
    super(operand, numbersInHex);
    variable = operand;
  }

  @SuppressWarnings("rawtypes") @Override public String tooltipText () {
    Instruction insn = getInstruction();
    BasicBlock basicBlock = getBasicBlock();
    Cfg cfg = getViewer().getCfg();

    // FIXME: actually we would need to know if the operand is a lhs or rhs
    // only then we can choose if it should be stateAfter or stateBefore
    Option<RootDomain> stateBefore = InstructionsHelper.getStateAfterInsn(insn, cfg, basicBlock);
    // TODO: we might want to show more things for the variable, like thresholds, congruences, etc.
    return InstructionsHelper.variableToHTML(stateBefore, variable);
  }

  private Instruction getInstruction () {
    // the instruction item is not always at the same level, so we have to
    // iterate over the chain of ancestors
    DrawableItem item = this;
    while (!(item instanceof InstructionItem)) {
      if (item == null)
        return null;
      item = item.getParent();
    }
    
    InstructionItem instructionItem = (InstructionItem) item;
    Instruction insn = instructionItem.getInstruction();
    return insn;
  }

  private BasicBlock getBasicBlock () {
    // the instruction item is not always at the same level, so we have to
    // iterate over the chain of ancestors
    DrawableItem item = this;
    while (!(item instanceof InstructionItem)) {
      if (item == null)
        return null;
      item = item.getParent();
    }
    
    InstructionItem instructionItem = (InstructionItem) item;
    BasicBlock basicBlock = ((BasicBlockItem) instructionItem.getParent()).getBasicBlock();
    return basicBlock;
  }

  @Override public JPopupMenu contextMenu (MouseEvent me, final Point2D relPos) {
    JPopupMenu menu = super.contextMenu(me, relPos);

    // create the popup menu object and append the menu items of the children widgets
    if (!root.slicingEnabled()) {
      JMenuItem slicingItem = new JMenuItem("Slice for " + variable);
      slicingItem.addActionListener(new ActionListener() {
        @Override public void actionPerformed (ActionEvent e) {
          RReil insn = ((LowLevelRReil) getInstruction()).toRReil();
          Cfg cfg = getViewer().getCfg();
          RReilCfg rreilCfg = (RReilCfg) cfg;
          AnalysisResult analysis = cfg.getAnalysis();
          Slice result = Slicer.slice(variable, insn, rreilCfg, analysis);
          if (result != null) {
            getViewer().applySlice(result);
            String message =
              "Showing sliced CFG for " + variable + " at " + insn.getRReilAddress().toStringWithHexPrefix();
            JCheckBox sliceMessageBox = new JCheckBox(message);
            sliceMessageBox.setSelected(true);
            sliceMessageBox.setBackground(ColorConfig.$SliceMessageBg);
            sliceMessageBox.addItemListener(new ItemListener() {
              @Override public void itemStateChanged (ItemEvent ie) {
                if (ie.getStateChange() == ItemEvent.DESELECTED) {
                  getViewer().disableSlice();
                  getViewer().removeFromToolbar("SliceMessage");
                }
              }
            });
            getViewer().addToToolbar("SliceMessage", sliceMessageBox);
          }
        }
      });
      menu.add(slicingItem);
    }
    return menu;
  }

}
