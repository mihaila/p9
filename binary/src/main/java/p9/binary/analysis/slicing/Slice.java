package p9.binary.analysis.slicing;

import java.util.Collection;
import java.util.Iterator;
import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeSet;

import rreil.lang.RReilAddr;

/**
 * A set of instruction addresses that are part of a program slice.
 *
 * @author Raphael Dümig
 */
public class Slice implements Iterable<RReilAddr> {
  private final NavigableSet<RReilAddr> addresses;

  public Slice () {
    this(new TreeSet<RReilAddr>());
  }

  public Slice (Set<RReilAddr> addresses) {
    this();
    addAll(addresses);
  }

  public Slice (NavigableSet<RReilAddr> addresses) {
    this.addresses = addresses;
  }

  @Override
  public Iterator<RReilAddr> iterator () {
    return addresses.iterator();
  }

  public int size () {
    return addresses.size();
  }

  public boolean isEmpty () {
    return addresses.isEmpty();
  }

  public boolean contains (RReilAddr address) {
    return addresses.contains(address);
  }

  public boolean add (RReilAddr addr) {
    return addresses.add(addr);
  }

  public boolean addAll (Collection<? extends RReilAddr> clctn) {
    return addresses.addAll(clctn);
  }

  public boolean remove (RReilAddr addr) {
    return addresses.remove(addr);
  }

}
